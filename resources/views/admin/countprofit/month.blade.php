@extends('admin.layout')
@section('title','BitBank | Monthly Profit')

@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE TABLE PORTLET-->
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-cogs"></i>Profit Payment
                            </div>
                        </div>
                        <div class="portlet-body flip-scroll">
                            <h4><strong>Number of records : </strong>{{ $deposits->total() }}</h4>
                            <table class="table table-bordered table-striped table-condensed flip-content text-center">
                                <thead class="flip-content">
                                <tr>
                                    <th width="20%"> Name</th>
                                    <th class="text-center"> Start Date</th>
                                    <th class="text-center"> End Date</th>
                                    <th class="text-center"> Last Pay Date</th>
                                    <th class="text-center"> Amount</th>
                                    <th class="text-center"> Rate</th>
                                    <th class="text-center"> Period (Days)</th>
                                    <th class="text-center"> Status</th>
                                    <th class="text-center"> BTC will be pay</th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($deposits as $deposit)
                                    <tr>
                                        <td class="text-left">{{ $deposit->user->getFullName() }}</td>
                                        <td>{{ $deposit->start_date }}</td>
                                        <td>{{ $deposit->end_date }}</td>
                                        <td>{{ $deposit->last_date_withdraw }}</td>
                                        <td>{{ $deposit->amount }}</td>
                                        <td>{{ $deposit->rate }}</td>
                                        <td>{{ $deposit->period }}</td>
                                        <td>{{ $deposit->status }}</td>
                                        <td>{{ $deposit->getProfit() }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{ $deposits->links() }}
                        </div>
                    </div>

                    <!-- END SAMPLE TABLE PORTLET-->
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
@endsection