@extends('admin.layout')
@section('title','Admin | Kick Deposit')
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE TABLE PORTLET-->
                    <div class="portlet box green">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-cogs"></i>Select Plan
                            </div>

                        </div>

                        <div class="portlet-body flip-scroll">
                            @if (session('error'))
                            <div class="panel panel-warning">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Error</h3>
                                </div>
                                <div class="panel-body">{{ session('error') }}</div>
                            </div>
                            @endif
                            
                            @if (session('message'))
                            <div class="panel panel-success">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Success</h3>
                                </div>
                                <div class="panel-body">{{ session('message') }}</div>
                            </div>
                            @endif
                            <form action="{{ route('admin.kickdeposit') }}" method="POST" class="form-horizontal form-bordered">
                                {!! csrf_field() !!}
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label col-md-3"><strong>Email</strong></label>
                                        <div class="col-md-4">
                                            <input name="email" type="email" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3"><strong>Amount</strong></label>
                                        <div class="col-md-4">
                                            <input name="amount" type="number" step="0.001" min="0"
                                                   max=""
                                                   class="form-control" id="maxlength_defaultconfig" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3"><strong>Plan</strong></label>
                                        <div class="col-md-4">
                                            <div class="mt-radio-list">
                                                <label class="mt-radio">
                                                    <input type="radio" name="optionsRadios21" id="btc_1"
                                                           value="0" checked=""> 0.2 - 1 BTC | Monthly profit : 20%
                                                    <span></span>
                                                </label>
                                                <label class="mt-radio">
                                                    <input type="radio" name="optionsRadios21" id="btc_1"
                                                           value="1" checked=""> 1 - 3 BTC | Monthly profit : 20%
                                                    <span></span>
                                                </label>
                                                <label class="mt-radio">
                                                    <input type="radio" name="optionsRadios21" id="btc_2"
                                                           value="3" checked=""> 3 - 5 BTC | Monthly profit : 20%
                                                    <span></span>
                                                </label>
                                                <label class="mt-radio">
                                                    <input type="radio" name="optionsRadios21" id="btc_3"
                                                           value="5" checked=""> 5 - 7 BTC | Monthly profit : 20%
                                                    <span></span>
                                                </label>
                                                <label class="mt-radio">
                                                    <input type="radio" name="optionsRadios21" id="btc_4"
                                                           value="7" checked=""> 7 - 10 BTC | Monthly profit : 20%
                                                    <span></span>
                                                </label>
                                                <label class="mt-radio">
                                                    <input type="radio" name="optionsRadios21" id="btc_5"
                                                           value="10" checked=""> >10 BTC | Monthly profit : 20%
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button class="btn blue btn-circle" type="submit" id="btn_submit" name="btn_submit" value="Kick">kick</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>


                    </div>
                    <!-- END SAMPLE TABLE PORTLET-->
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
@endsection