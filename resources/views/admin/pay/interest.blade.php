@extends('admin.layout')
@section('title','BitBank | Pay Monthly Interest')

@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    @if(Session::has('status'))
                        <div class="alert alert-{{ Session::get('status') }}" id="alert-payment">
                            <p>{{ Session::get('message') }}</p>
                        </div>
                    @endif
                    <h4><strong>Number of records : </strong>{{ $deposits->total() }}</h4>
                    <!-- BEGIN SAMPLE TABLE PORTLET-->
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-cogs"></i>Pay Monthly Interest
                            </div>
                        </div>
                        <div class="portlet-body flip-scroll">
                            <table class="table table-bordered table-striped table-condensed flip-content text-center">
                                <thead class="flip-content">
                                <tr>
                                    <th class="text-center" style="vertical-align: middle;">Name</th>
                                    <th class="text-center" style="vertical-align: middle;">Start Date</th>
                                    <th class="text-center" style="vertical-align: middle;">Last Pay Date</th>
                                    <th class="text-center" style="vertical-align: middle;">Pay at</th>
                                    <th class="text-center" style="vertical-align: middle;">Amount</th>
                                    <th class="text-center" style="vertical-align: middle;">Type</th>
                                    <th class="text-center" style="vertical-align: middle;">Status</th>
                                    <th class="text-center" style="vertical-align: middle;">BTC will be paid</th>
                                    <th class="text-center" style="vertical-align: middle;">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($deposits as $deposit)
                                <tr>
                                <td style="vertical-align: middle;">{{ $deposit->user->getFullName() }}</td>
                                    <td style="vertical-align: middle;">{{ $deposit->getStartDate('d-m-Y') }}</td>
                                    <td style="vertical-align: middle;">{{ $deposit->getLastPayDate('d-m-Y') }}</td>
                                    <td style="vertical-align: middle;">{{ $deposit->getPayDate('d-m-Y') }}</td>
                                    <td style="vertical-align: middle;">{{ $deposit->amount }}</td>
                                    @if($deposit->type == 0)
                                    <td style="vertical-align: middle;">0.2 BTC</td>
                                    @else
                                    <td style="vertical-align: middle;">{{ $deposit->type }} BTC</td>
                                    @endif
                                    <td style="vertical-align: middle;">{{ $deposit->status }}</td>
                                    <td style="vertical-align: middle;">{{ $deposit->getInterest() }}</td>
                                    <td style="vertical-align: middle;">
                                        <form style="margin:0;padding: 0;" method="post"
                                              action="{{ route('admin.pay.interest.withdraw', ['id' => $deposit->id]) }}">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="amount"
                                                   value="{{ $deposit->amount }}">
                                            <input type="hidden" name="date_pay"
                                                   value="{{ $deposit->getPayDate('Y-m-d') }}">
                                            <input type="hidden" name="user_id"
                                                   value="{{ $deposit->user_id }}">
                                            <button class="btn btn-primary btn-sm">Pay</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END SAMPLE TABLE PORTLET-->
                    {{ $deposits->links() }}
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
@endsection
@section('script')
    <script type="application/javascript">
        $(document).ready(function () {
            setTimeout(function () {
                $('#alert-payment').fadeOut(1000);
            }, 2000);
        });
    </script>
@endsection