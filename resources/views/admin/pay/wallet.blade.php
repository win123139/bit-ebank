@extends('admin.layout')
@section('title','BitBank | Withdraw From Wallet')

@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    @if(Session::has('status'))
                        <div class="alert alert-{{ Session::get('status') }}" id="alert-payment">
                            <p>{{ Session::get('message') }}</p>
                        </div>
                    @endif
                        <h4><strong>Number of records : </strong>{{ $transactions->total() }}</h4>
                <!-- BEGIN SAMPLE TABLE PORTLET-->
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-cogs"></i>Withdraw From Wallet
                            </div>
                        </div>
                        <div class="portlet-body flip-scroll">
                            <table class="table table-bordered table-striped table-condensed flip-content text-center">
                                <thead class="flip-content">
                                <tr>
                                    <th class="text-center" style="vertical-align: middle;">Name</th>
                                    <th class="text-center" style="vertical-align: middle;">Date</th>
                                    <th class="text-center" style="vertical-align: middle;">Type</th>
                                    <th class="text-center" style="vertical-align: middle;">Wallet</th>
                                    <th class="text-center" style="vertical-align: middle;">Amount</th>
                                    <th class="text-center" style="vertical-align: middle;">Status</th>
                                    <th class="text-center" style="vertical-align: middle;">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($transactions as $transaction)
                                    <tr>
                                        <td style="vertical-align: middle;">{{ $transaction->user->getFullName() }}</td>
                                        <td style="vertical-align: middle;">{{ $transaction->date }}</td>
                                        <td style="vertical-align: middle;">{{ $transaction->type }}</td>
                                        <td style="vertical-align: middle;">{{ $transaction->receive_wallet }}</td>
                                        <td style="vertical-align: middle;">{{ $transaction->amount }}</td>
                                        <td style="vertical-align: middle;">{{ $transaction->status }}</td>
                                        <td style="vertical-align: middle;">
                                            <form style="margin:0;padding: 0;" method="post"
                                                  action="{{ route('admin.pay.wallet.withdraw', ['id' => $transaction->id]) }}">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="amount"
                                                       value="{{ $transaction->amount }}">
                                                <input type="hidden" name="receive_wallet"
                                                       value="{{ $transaction->receive_wallet }}">
                                                <button class="btn btn-primary btn-sm">Accept Withdraw</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END SAMPLE TABLE PORTLET-->
                    {{ $transactions->links() }}
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
@endsection
@section('script')
    <script type="application/javascript">
        $(document).ready(function () {
            setTimeout(function () {
                $('#alert-payment').fadeOut(1000);
            }, 2000);
        });
    </script>
@endsection