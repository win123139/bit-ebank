@extends('admin.layout')
@section('title','Admin | Withdraw To Personal Wallet')
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE TABLE PORTLET-->
                    <div class="portlet box green">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-cogs"></i>Select Amount To Withdraw
                            </div>

                        </div>

                        <div class="portlet-body flip-scroll">
                            @if (session('error'))
                            <div class="panel panel-warning">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Error</h3>
                                </div>
                                <div class="panel-body">{{ session('error') }}</div>
                            </div>
                            @endif
                            
                            @if (session('message'))
                            <div class="panel panel-success">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Success</h3>
                                </div>
                                <div class="panel-body">{{ session('message') }}</div>
                            </div>
                            @endif
                            <form action="{{ route('admin.transfer') }}" method="POST" class="form-horizontal form-bordered">
                                {!! csrf_field() !!}
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label col-md-3"><strong>Balance:</strong></label>
                                        <div class="col-md-4">
                                            <label class="control-label">{{ $balance }}</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3"><strong>System wallet:</strong></label>
                                        <div class="col-md-4">
                                            <label class="control-label">{{ env('BLOCKIO_ADDRESS') }}</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3"><strong>Personal wallet:</strong></label>
                                        <div class="col-md-4">
                                            <label class="control-label">{{ env('PRIVATE_WALLET') }}</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3"><strong>Amount</strong></label>
                                        <div class="col-md-4">
                                            <input type="number" step="0.0001" name="amount" id="amount" required class="form-control input-md">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button class="btn blue btn-circle" type="submit" id="btn_submit" name="btn_submit" value="withdraw">Withdraw</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>


                    </div>
                    <!-- END SAMPLE TABLE PORTLET-->
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
@endsection