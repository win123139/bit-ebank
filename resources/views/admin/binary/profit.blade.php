@extends('admin.layout')
@section('title','BitBank | Binary Profit')

@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE TABLE PORTLET-->
                    @if(Session::has('status'))
                        <div class="alert alert-{{ Session::get('status') }}" id="alert-payment">
                            <p>{{ Session::get('message') }}</p>
                        </div>
                    @endif
                    <h4><strong>Number of records : </strong>{{ $users->total() }}</h4>
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-cogs"></i>Profit Payment
                            </div>
                        </div>
                        <div class="portlet-body flip-scroll">
                            <table class="table table-bordered table-striped table-condensed flip-content text-center">
                                <thead class="flip-content">
                                <tr>
                                    <th class="text-center" style="vertical-align: middle;">Name</th>
                                    <th class="text-center" style="vertical-align: middle;">Directly member</th>
                                    <th class="text-center" style="vertical-align: middle;">Last Left Revenue</th>
                                    <th class="text-center" style="vertical-align: middle;">Last Right Revenue</th>
                                    <th class="text-center" style="vertical-align: middle;">Left Revenue</th>
                                    <th class="text-center" style="vertical-align: middle;">Right Revenue</th>
                                    <th class="text-center" style="vertical-align: middle;">This month count by</th>
                                    <th class="text-center" style="vertical-align: middle;">Amount Payment</th>
                                    <th class="text-center" style="vertical-align: middle;">Last date Payment</th>
                                    <th class="text-center" style="vertical-align: middle;">Action</th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $user)
                                    @php
                                        if(!is_null($user->binaryDownline)){
                                            $leftdeposit = $user->binaryDownline->getLeftDeposit();
                                            $rightdeposit = $user->binaryDownline->getRightDeposit();
                                            if($leftdeposit<=$rightdeposit){
                                                $countby = 'Left';
                                                $payamount = $leftdeposit - $user->getBinaryLastLeftVenue();
                                            }else{
                                                $countby = 'Right';
                                                $payamount = $rightdeposit - $user->getBinaryLastRightVenue();
                                            }
                                        } else {
                                            $leftdeposit = 0;
                                            $rightdeposit = 0;
                                            $countby = 'Left';
                                            $payamount = 0;
                                        }
                                        $leftdeposit = number_format($leftdeposit,2);
                                        $rightdeposit = number_format($rightdeposit,2);
                                        $payamount = $payamount*10/100;

                                    @endphp
                                    <tr>
                                        <td>{{ $user->getFullName() }}</td>
                                        <td>{{ $user->getReferrals()->count() }}</td>
                                        <td>{{ $user->getBinaryLastLeftVenue() }}</td>
                                        <td>{{ $user->getBinaryLastRightVenue() }}</td>
                                        <td>{{ $leftdeposit }}</td>
                                        <td>{{ $rightdeposit }}</td>
                                        <td>{{ $countby }}</td>
                                        <td>{{ $payamount }}</td>
                                        <td>{{ is_null($user->binaryTemp) ? 0 : $user->binaryTemp->getLastDatePayMent('d-m-Y') }}</td>
                                        <td style="vertical-align: middle;">
                                            <form style="margin:0;padding: 0;" method="post"
                                                  action="{{ route('admin.binary.payment',['id' => $user->id]) }}">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="user_id"
                                                       value="{{ $user->id }}">
                                                <input type="hidden" name="accountno"
                                                       value="{{ $user->accountno }}">
                                                <input type="hidden" name="payamount"
                                                       value="{{ $payamount }}">
                                                <input type="hidden" name="leftdeposit"
                                                       value="{{ $leftdeposit }}">
                                                <input type="hidden" name="rightdeposit"
                                                       value="{{ $rightdeposit }}">
                                                <button class="btn btn-primary btn-sm">Pay</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{ $users->links() }}
                        </div>
                    </div>

                    <!-- END SAMPLE TABLE PORTLET-->
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
@endsection
@section('script')
    <script type="application/javascript">
        $(document).ready(function () {
            setTimeout(function () {
                $('#alert-payment').fadeOut(1000);
            }, 2000);
        });
    </script>
@endsection