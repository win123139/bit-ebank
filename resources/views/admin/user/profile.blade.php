@extends('admin.layout')
@section('title','BitBank | Admin Profile')
@section('style')
    <link href="{{ asset('assets/pages/css/profile.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection
@section('body-class','page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-content-white')
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title"> User Profile | Account

            </h1>
            <!-- END PAGE TITLE-->
            <!-- END PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PROFILE SIDEBAR -->
                    <div class="profile-sidebar">
                        <!-- PORTLET MAIN -->
                        <div class="portlet light profile-sidebar-portlet ">
                            <!-- SIDEBAR USERPIC -->
                            <div class="profile-userpic">
                                <img src="{{ asset('assets/pages/media/profile/blank_profile.png') }}"
                                     class="img-responsive" alt=""></div>
                            <!-- END SIDEBAR USERPIC -->
                            <!-- SIDEBAR USER TITLE -->
                            <div class="profile-usertitle">
                                <div class="profile-usertitle-name">{{ Auth::user()->getFullName() }}</div>
                            </div>
                            <!-- END SIDEBAR USER TITLE -->
                            <!-- SIDEBAR BUTTONS -->

                            <!-- END SIDEBAR BUTTONS -->
                            <!-- SIDEBAR MENU -->
                            <div class="profile-usermenu">
                                <ul class="nav">
                                    <li>
                                        <a href="{{ route('admin.user.overview') }}">
                                            <i class="icon-home"></i> Overview </a>
                                    </li>
                                    <li class="active">
                                        <a href="{{ route('admin.user.profile') }}">
                                            <i class="icon-settings"></i> Account Settings </a>
                                    </li>

                                </ul>
                            </div>
                            <!-- END MENU -->
                        </div>
                        <!-- END PORTLET MAIN -->

                    </div>
                    <!-- END BEGIN PROFILE SIDEBAR -->
                    <!-- BEGIN PROFILE CONTENT -->
                    <div class="profile-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light ">
                                    <div class="portlet-title tabbable-line">
                                        <div class="caption caption-md">
                                            <i class="icon-globe theme-font hide"></i>
                                            <span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
                                        </div>
                                        <ul class="nav nav-tabs">
                                            <li class="{{ Session::has('password')? '' : 'active' }}">
                                                <a href="#tab_1_1" data-toggle="tab">Personal Info</a>
                                            </li>

                                            <li class="{{ Session::has('password')? 'active' : '' }}">
                                                <a href="#tab_1_3" data-toggle="tab">Change Password</a>
                                            </li>

                                        </ul>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="tab-content">
                                            <!-- PERSONAL INFO TAB -->
                                            <div class="tab-pane {{ Session::has('password')? '' : 'active' }}"
                                                 id="tab_1_1">
                                                <form role="form" action="{{ route('admin.user.profile.update') }}"
                                                      novalidate
                                                      method="POST">
                                                    {{ csrf_field() }}
                                                    {{ method_field('PUT') }}
                                                    @if ($errors->any() && !Session::has('password'))
                                                        <div class="alert alert-danger" role="alert">
                                                            {{ $errors->first() }}
                                                        </div>
                                                    @endif
                                                    @if (Session::has('profile'))
                                                        <div class="alert alert-{{ Session::get('profile') }}" role="alert">
                                                            {{ Session::get('message') }}
                                                        </div>
                                                    @endif
                                                    <div class="form-group {{ $errors->has('firstname') ? 'has-error' : '' }}">
                                                        <label class="control-label">First Name</label>
                                                        <input type="text" required id="firstname" name="firstname"
                                                               value="{{ Auth::user()->firstname }}"
                                                               class="form-control"/>
                                                    </div>
                                                    <div class="form-group {{ $errors->has('lastname') ? 'has-error' : '' }}">
                                                        <label class="control-label">Last Name</label>
                                                        <input type="text" required id="lastname" name="lastname"
                                                               value="{{ Auth::user()->lastname }}"
                                                               class="form-control"/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">CCno</label>
                                                        <input type="text" required readonly
                                                               value="{{ Auth::user()->ccno }}"
                                                               class="form-control" {{ Auth::user()->ccno }}/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">Email</label>
                                                        <input type="text" readonly required
                                                               value="{{ Auth::user()->email }}"
                                                               class="form-control"/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">Country</label>
                                                        <input type="text" readonly required
                                                               value="{{ Auth::user()->country }}"
                                                               class="form-control"/>
                                                    </div>
                                                    <div class="form-group {{ $errors->has('birthday') ? 'has-error' : '' }}">
                                                        <label class="control-label">Birthday</label>
                                                        <input type="date" id="birthday" name="birthday" required
                                                               value="{{ Auth::user()->created_date }}"
                                                               class="form-control"/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">Your sponsor</label>
                                                        <input type="text" required readonly
                                                               value="{{ Auth::user()->referral }}"
                                                               class="form-control"/>
                                                    </div>

                                                    <div class="margiv-top-10">
                                                        <input value="Save Changes" class="btn btn-primary"
                                                               id="btn_update" name="btn_update" type="submit">
                                                    </div>
                                                </form>
                                            </div>
                                            <!-- END PERSONAL INFO TAB -->

                                            <!-- CHANGE PASSWORD TAB -->
                                            <div class="tab-pane {{ Session::has('password')? 'active' : '' }}"
                                                 id="tab_1_3">
                                                <form action="{{ route('admin.user.profile.password') }}"
                                                      method="POST" novalidate>
                                                    {{ csrf_field() }}
                                                    {{ method_field('PUT') }}
                                                    @if (Session::has('password'))
                                                        @if ($errors->any())
                                                        <div class="alert alert-danger" role="alert">
                                                            {{ $errors->first() }}
                                                        </div>
                                                        @else
                                                        <div class="alert alert-{{ Session::get('password') }}" role="alert">
                                                           {{ Session::get('message') }}
                                                        </div>
                                                        @endif
                                                    @endif
                                                    <div class="form-group {{ ($errors->has('current_pass') || Session::has('password')) ? 'has-error' : '' }}">
                                                        <label class="control-label">Current Password</label>
                                                        <input type="password" id="current_pass" name="current_pass"
                                                               class="form-control"/></div>
                                                    <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                                                        <label class="control-label">New Password</label>
                                                        <input type="password" id="password" name="password"
                                                               class="form-control"/></div>
                                                    <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                                                        <label class="control-label">Re-type New Password</label>
                                                        <input type="password" id="password_confirmation"
                                                               name="password_confirmation"
                                                               class="form-control"/></div>
                                                    <div class="margin-top-10">

                                                        <button type="submit" class="btn blue" id="btn_submit">Change
                                                            Password
                                                        </button>
                                                        <a href="{{ route('admin.user.profile') }}"
                                                           class="btn default"> Cancel </a>
                                                    </div>
                                                </form>
                                            </div>
                                            <!-- END CHANGE PASSWORD TAB -->

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PROFILE CONTENT -->
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
@endsection