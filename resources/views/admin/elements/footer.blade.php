<div class="page-footer">
    <div class="page-footer-inner"> 2015 &copy; Singapore National Bank
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>