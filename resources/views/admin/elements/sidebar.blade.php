<div class="page-sidebar-wrapper">
    <!-- BEGIN SIDEBAR -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <ul class="page-sidebar-menu  page-header-fixed" data-keep-expanded="false" data-auto-scroll="true"
            data-slide-speed="200" style="padding-top: 20px">
            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
            <li class="sidebar-toggler-wrapper hide">
                <div class="sidebar-toggler">
                    <span></span>
                </div>
            </li>
            <!-- END SIDEBAR TOGGLER BUTTON -->
            <li class="nav-item {{ Request::is('admin') ? 'open active' : '' }}">
                <a href="{{ route('admin.home') }}" class="nav-link nav-toggle">
                    <i class="icon-home"></i>
                    <span class="title" href="personal.php">Admin Page</span>
                    @if(Request::is('admin'))
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    @else
                        <span class="arrow"></span>
                    @endif
                </a>

            </li>
            <li class="heading">
                <h3 class="uppercase">Features</h3>
            </li>
            <li class="nav-item {{ Request::is('admin/kickdeposit') ? 'open active' : '' }}">
                <a href="{{ route('admin.kickdeposit') }}" class="nav-link nav-toggle">
                    <i class="icon-size-actual"></i>
                    <span class="title">Kick Deposit</span>
                    @if(Request::is('admin/kickdeposit'))
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    @else
                        <span class="arrow"></span>
                    @endif
                </a>
            </li>

            <li class="nav-item {{ Request::is('admin/countprofit/monthly') ? 'open active' : '' }}">
                <a href="{{ route('admin.countprofit') }}" class="nav-link nav-toggle">
                    <i class="icon-calculator"></i>
                    <span class="title">Count Profit Monthly</span>
                    @if(Request::is('admin/countprofit/monthly'))
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    @else
                        <span class="arrow"></span>
                    @endif
                </a>
            </li>

            <li class="nav-item {{ Request::is('admin/pay*') ? 'open active' : '' }}">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-share-alt"></i>
                    <span class="title">Pay</span>
                    @if(Request::is('admin/pay*'))
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    @else
                        <span class="arrow"></span>
                    @endif
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  {{ Request::is('admin/pay/interest') ? 'open active' : '' }}">
                        <a href="{{ route('admin.pay.interest') }}" class="nav-link ">
                            <i class="icon-bar-chart"></i>
                            <span class="title">Interest</span>
                            @if(Request::is('admin/pay/interest'))
                                <span class="selected"></span>
                                <span class="arrow open"></span>
                            @else
                                <span class="arrow"></span>
                            @endif
                        </a>
                    </li>
                    <li class="nav-item  {{ Request::is('admin/pay/profit') ? 'open active' : '' }}">
                        <a href="{{ route('admin.pay.profit') }}" class="nav-link ">
                            <i class="icon-bar-chart"></i>
                            <span class="title">Profit</span>
                            @if(Request::is('admin/pay/profit'))
                                <span class="selected"></span>
                                <span class="arrow open"></span>
                            @else
                                <span class="arrow"></span>
                            @endif
                        </a>
                    </li>
                    <li class="nav-item  {{ Request::is('admin/pay/wallet') ? 'open active' : '' }}">
                        <a href="{{ route('admin.pay.wallet') }}" class="nav-link ">
                            <i class="icon-wallet"></i>
                            <span class="title">Wallet</span>
                            @if(Request::is('admin/pay/wallet'))
                                <span class="selected"></span>
                                <span class="arrow open"></span>
                            @else
                                <span class="arrow"></span>
                            @endif
                        </a>
                    </li>
                </ul>
            </li>

            <li class="nav-item {{ Request::is('admin/binary') ? 'open active' : '' }}">
                <a href="{{ route('admin.binary') }}" class="nav-link nav-toggle">
                    <i class="icon-bubbles"></i>
                    <span class="title">Binary</span>
                    @if(Request::is('admin/binary'))
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    @else
                        <span class="arrow"></span>
                    @endif
                </a>
            </li>
            
            <li class="nav-item {{ Request::is('admin/transfer') ? 'open active' : '' }}">
                <a href="{{ route('admin.transfer') }}" class="nav-link nav-toggle">
                    <i class="glyphicon glyphicon-transfer"></i>
                    <span class="title">Personal Withdraw</span>
                    @if(Request::is('admin/binary'))
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    @else
                        <span class="arrow"></span>
                    @endif
                </a>
            </li>

            <li class="heading">
                <h3 class="uppercase">Admin Profile</h3>
            </li>

            <li class="nav-item {{ Request::is('admin/user*') ? 'open active' : '' }}">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-user"></i>
                    <span class="title">Admin</span>
                    @if(Request::is('admin/user*'))
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    @else
                        <span class="arrow"></span>
                    @endif
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  {{ Request::is('admin/user/overview') ? 'open active' : '' }}">
                        <a href="{{ route('admin.user.overview') }}" class="nav-link ">
                            <i class="icon-user"></i>
                            <span class="title">Overview</span>
                            @if(Request::is('admin/user/overview'))
                                <span class="selected"></span>
                                <span class="arrow open"></span>
                            @else
                                <span class="arrow"></span>
                            @endif
                        </a>
                    </li>
                    <li class="nav-item  {{ Request::is('admin/user/profile') ? 'open active' : '' }}">
                        <a href="{{ route('admin.user.profile') }}" class="nav-link ">
                            <i class="icon-user-female"></i>
                            <span class="title">Account Setting</span>
                            @if(Request::is('admin/user/profile'))
                                <span class="selected"></span>
                                <span class="arrow open"></span>
                            @else
                                <span class="arrow"></span>
                            @endif
                        </a>
                    </li>

                    <li class="nav-item  ">
                        <a href="{{ route('auth.logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"
                           class="nav-link nav-toggle">
                            <i class="icon-notebook"></i>
                            <span class="title">Logout</span>
                            <span class="arrow"></span>
                        </a>
                    </li>

                </ul>
            </li>
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>