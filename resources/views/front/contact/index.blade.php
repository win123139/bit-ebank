@extends('front.layout')
@section('title','Bitbank Express')
@section('main-content')
    <aside id="fh5co-hero" class="js-fullheight">
        <div class="flexslider js-fullheight">
            <ul class="slides">
                <li style="background-image: url({{ asset('images/slide_3.jpg') }});">
                    <div class="overlay-gradient"></div>
                    <div class="container">
                        <div class="col-md-10 col-md-offset-1 text-center js-fullheight slider-text">
                            <div class="slider-text-inner">
                                <h2>Get in touch. Don't be shy.</h2>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </aside>

    <div class="fh5co-contact animate-box">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <h3>Contact Info.</h3>
                    <ul class="contact-info">
                        <li><i class="icon-map"></i>321-323 Orchard Rd</li>
                        <li><i class="icon-phone"></i>+65 215 215 428</li>
                        <li><i class="icon-envelope"></i><a href="#">info@bit-ebank.com</a></li>
                        <li><i class="icon-globe"></i><a href="#">https://bit-ebank.com</a></li>
                    </ul>
                </div>
                <div class="col-md-8 col-md-push-1 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input class="form-control" placeholder="Name" type="text">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input class="form-control" placeholder="Email" type="text">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <textarea name="" class="form-control" id="" cols="30" rows="7" placeholder="Message"></textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <input value="Send Message" class="btn btn-primary" type="submit">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="map" class="animate-box" data-animate-effect="fadeIn"></div>
@endsection