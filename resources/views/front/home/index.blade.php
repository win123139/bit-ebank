@extends('front.layout')
@section('title','Bitbank Express')
@section('main-content')
    <aside id="fh5co-hero" class="js-fullheight">
        <div class="flexslider js-fullheight">
            <ul class="slides">
                <li style="background-image: url({{ asset('images/slide_1.jpg') }});">
                    <div class="overlay-gradient"></div>
                    <div class="container">
                        <div class="col-md-10 col-md-offset-1 text-center js-fullheight slider-text">
                            <div class="slider-text-inner">
                                <h2>Make deposit now and get Monthly profit</h2>
                                <p><a href="{{ route('front.investment') }}#investment" class="btn btn-primary btn-lg">Deposit now
                                        !</a></p>
                            </div>
                        </div>
                    </div>
                </li>
                <li style="background-image: url({{ asset('images/slide_3.jpg') }});">
                    <div class="container">
                        <div class="col-md-10 col-md-offset-1 text-center js-fullheight slider-text">
                            <div class="slider-text-inner">
                                <h2>Become richer with us</h2>
                                <p><a href="{{ route('front.investment') }}#investment" class="btn btn-primary btn-lg">Make
                                        deposit NOW!</a></p>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </aside>
    <div id="fh5co-why-us" class="animate-box" style="padding: 50px;">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 text-center fh5co-heading">
                    <h2>Why Choose Us</h2>
                    <p>SAFE - STABLE - SECURE - That values we will take to you</p>
                </div>
                <div class="col-md-4 text-center item-block">
                    <span class="icon"><img src="{{ asset('images/30.svg') }}" alt="Free HTML5 Templates"
                                            class="img-responsive"></span>
                    <h3>Strategy</h3>
                    <p>With our plans, your investment will increase monthly, 24h in a day, 30 days in a month and 12
                        months in a year.</p>
                    <p><a href="#" class="btn btn-primary btn-outline with-arrow">Learn more <i
                                    class="icon-arrow-right"></i></a></p>
                </div>
                <div class="col-md-4 text-center item-block">
                    <span class="icon"><img src="{{ asset('images/18.svg') }}" alt="Free HTML5 Templates"
                                            class="img-responsive"></span>
                    <h3>Explore</h3>
                    <p>Your money and transactions will be kept in safe. By the most security system and best people in
                        IT field.</p>
                    <p><a href="#" class="btn btn-primary btn-outline with-arrow">Learn more <i
                                    class="icon-arrow-right"></i></a></p>
                </div>
                <div class="col-md-4 text-center item-block">
                    <span class="icon"><img src="{{ asset('images/27.svg') }}" alt="Free HTML5 Templates"
                                            class="img-responsive"></span>
                    <h3>Expertise</h3>
                    <p>The highest interest rates, exchange rates and many deposit plans. Your profit is up to you.</p>
                    <p><a href="#" class="btn btn-primary btn-outline with-arrow">Learn more <i
                                    class="icon-arrow-right"></i></a></p>
                </div>
            </div>
        </div>
    </div>
    <div id="map" class="animate-box" data-animate-effect="fadeIn"></div>
    <div id="fh5co-blog" class="animate-box">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 text-center fh5co-heading">
                    <h2>Latest <em>from</em> Bitbank</h2>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there
                        live Bitbank. </p>
                </div>
            </div>
        </div>
    </div>
@endsection

