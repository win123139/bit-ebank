@extends('front.layout')
@section('title','Bitbank Express')
@section('main-content')
    <aside id="fh5co-hero" class="js-fullheight">
        <div class="flexslider js-fullheight">
            <ul class="slides">
                <li style="background-image: url({{ asset('images/slide_1.jpg') }});">
                    <div class="overlay-gradient"></div>
                    <div class="container">
                        <div class="col-md-10 col-md-offset-1 text-center js-fullheight slider-text">
                            <div class="slider-text-inner">
                                <h2>Let's follow our plan!</h2>
                                <p class="fh5co-lead">We always receive Investment deposit & <a href="#">Earning monthly</a> for EVERY people<i class="icon-heart"> </i></p>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>.
        </div>
    </aside>


    <div class="fh5co-pricing">
        <div class="" id="investment">


            <div class="row">
                <div class="col-md-6 col-md-offset-3 text-center fh5co-heading animate-box">
                    <h2>Investment Plans</h2>
                    <p>By Investing in our Plan as follow, you will receive the profit monthly.</p>
                </div>
                <div class="pricing col-md-12">
                    <div class="col-md-1 ">
                        
                    </div>
                    <div class="col-md-2 animate-box">
                        <div class="price-box">
                            <h2 class="pricing-plan">Standard</h2>
                            <div class="price"> <sup class="currency" style=" font-size: 25px;"> 0.2 - 1 BTC </sup> </div>
                            

                                Monthly profit:<br/>20%<br/>
                                Max Commission:<br/> 500$/month <br/>
                                
                                <a href="#">PRINCIPAL RETURN</a></p>
                            <a href="#" class="btn btn-select-plan btn-sm">Select Plan</a>
                        </div>
                    </div>
                    <div class="col-md-2 animate-box">
                        <div class="price-box">
                            <h2 class="pricing-plan">Iron</h2>
                            <div class="price"> <sup class="currency" style=" font-size: 25px;"> 1 - 3 BTC </sup> </div>
                            

                                Monthly profit:<br/> 20%<br/>
                                Max Commission:<br/> 2000$/month <br/>
                                
                                <a href="#">PRINCIPAL RETURN</a></p>
                            <a href="#" class="btn btn-select-plan btn-sm">Select Plan</a>
                        </div>
                    </div>
                    <div class="col-md-2 animate-box">
                        <div class="price-box">
                            <h2 class="pricing-plan">Bronze</h2>
                            <div class="price"> <sup class="currency" style=" font-size: 25px;"> 3 - 5 BTC </sup> </div>
                            

                                Monthly profit:<br/> 20%<br/>
                                Max Commission:<br/> 5000$/month <br/>
                                
                                <a href="#">PRINCIPAL RETURN</a></p>
                            <a href="#" class="btn btn-select-plan btn-sm">Select Plan</a>
                        </div>
                    </div>

                    <div class="col-md-2 animate-box">
                        <div class="price-box">
                            <h2 class="pricing-plan">Silver</h2>
                            <div class="price"> <sup class="currency" style=" font-size: 25px;"> 5 - 7 BTC </sup> </div>
                            

                                Monthly profit:<br/> 20%<br/>
                                Max Commission:<br/> 10000$/month <br/>
                                
                                <a href="#">PRINCIPAL RETURN</a></p>
                            <a href="#" class="btn btn-select-plan btn-sm">Select Plan</a>
                        </div>
                    </div>

                    <div class="col-md-2 animate-box">
                        <div class="price-box">
                            <h2 class="pricing-plan">Gold</h2>
                            <div class="price"> <sup class="currency" style=" font-size: 25px;"> 7 - 10 BTC </sup> </div>
                            

                                Monthly profit:<br/> 20%<br/>
                                Max Commission:<br/> 20000$/month <br/>
                                
                                <a href="#">PRINCIPAL RETURN</a></p>
                            <a href="#" class="btn btn-select-plan btn-sm">Select Plan</a>
                        </div>
                    </div>
                </div>

                <div class="pricing col-md-12">
                    <div class="col-md-1 ">
                        
                    </div>
                    <div class="col-md-2 animate-box">
                        <div class="price-box">
                            <h2 class="pricing-plan">Diamond</h2>
                            <div class="price"> <sup class="currency" style=" font-size: 25px;"> > 10 BTC </sup> </div>
                            

                                Monthly profit:<br/> 20%<br/>
                                Max Commission:<br/> 50000$/month <br/>
                                
                                <a href="#">PRINCIPAL RETURN</a></p>
                            <a href="#" class="btn btn-select-plan btn-sm">Select Plan</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 col-md-offset-3 animate-box">
                    <div class="col-md-12" style="position: absolute; left: 13%; z-index:999;">
                        <p>Register and Invest in us as soon as you can <a href="#">Start Invest!</a></p>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="fh5co-cta" style="background-image: url({{ asset('images/image_3.jpg') }});">
        <div class="overlay"></div>
        <div class="container">
            <div class="col-md-12 text-center animate-box">
                <h3>We are trying our best to update system daily</h3>
                <p><a href="{{ route('auth.register.form') }}" class="btn btn-primary btn-outline with-arrow">Join us ! <i class="icon-arrow-right"></i></a></p>
            </div>
        </div>
    </div>

@endsection