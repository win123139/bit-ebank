@extends('front.layout')
@section('title','Bitbank Express')
@section('main-content')
    <aside id="fh5co-hero" class="js-fullheight">
        <div class="flexslider js-fullheight">
            <ul class="slides">
                <li style="background-image: url({{ asset('images/slide_3.jpg') }});">
                    <div class="overlay-gradient"></div>
                    <div class="container">
                        <div class="col-md-10 col-md-offset-1 text-center js-fullheight slider-text">
                            <div class="slider-text-inner">
                                <h2>Register Now to get your BTC wallet</h2>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </aside>

    <div id="tems_use" class="fh5co-contact animate-box">
        <div class="container">
            <div class="row">
                <div  class="col-md-8 col-md-push-1 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0" style="left: 15%;" >
                    <div class="row">
                        <h2 style="margin-left: 42%;">Terms Of Use</h2>
                        <div class="col-md-12">
                            <h3>General Terms</h3>
                            <p>1.This agreement with the Client is between Bitbank-EXPRESS LIMITED investment company Bitbank-express (the ‘company’), established in full compliance with the laws of the United Kingdom, and the individual (the ‘client’).</p>
                            <p>2. All funds are accepted and paid via Bitcoin crypto-currency only. The Company may add other payment methods and/or digital currencies at any time at its sole discretion.</p>
                            <p>3. Minimum investment requirement is 0.001 BTC. There is no maximum limit per deposit and you may make as many additional investments as you wish.</p>
                            <p>4. The Client is provided with a hourly interest at variable rate that depends on the size of investment. Interest is paid on hourly basis, 7 days a week.</p>
                            <p>5. The client can reinvest or withdraw his/her earnings at any time.</p>
                            <p>6. All withdrawals are processed instantly, however sometimes it can take up to 24 hours because of security reasons, or while we are performing server or system upgrades. You do not have to contact support regarding your withdrawal request; it will be processed in case it was not processed instantly, within 24 business hours.</p>
                            <p>7. The Client is eligible for a three level referral commission. The Company shall pay the Client 5% affiliate commission for every investment made by the Client's referred person. Affiliate commission is paid directly into the Client's account balance at the Company. All additional investments made by the Client's referred person will generate further 5% commission.</p>
                            <p>8. All interest income and funds withdrawal shall be paid TAX-FREE at source. The Client understands and agrees that it is the Client's responsibility to comply with any laws or regulations regarding the establishment of an account or any interest and withdrawal paid thereon in the Client's domicile or legal jurisdiction.</p>
                            <p>9. All items are credited to the account conditionally and are subject to collection by the Company. The Client shall be responsible for any exchange and handling fees, which may be incurred in connection with any item and such fees shall be for account of the Client.</p>
                            <p>10. The offer and acceptance of the investments provided for herein may be prohibited or limited in certain jurisdictions. It is understood that it is the responsibility of the Client, or any person who is considering making a investment in the Company, to inform himself regarding, and to comply with, all the legal provisions and regulations in force in his jurisdiction with respect to the making and delivery of the investment, exchange controls, taxes and similar matters.</p>
                            <p>11. The Client must be legal age in their country to open an account at the Company, and in all the cases the Client's minimal age must be 18 years.</p>
                            <p>12. If the Client does not agree with the current account balance information relating to the account transactions, he/she must inform the Company immediately. If appropriate, the Client should send a claim to the Company enclosing the copies of the necessary documents in the event of a non-acknowledged transaction, a failed transaction or a problem concerning the settlement of a transaction.</p>
                            <p>13. The Company reserves the right not to act on instructions provided by the Client if the Company is not satisfied as to their authenticity. The Company reserves the right to contact the Client regarding withdrawal requests or other transactions in order to perform security checks. The Company will not be liable or responsible for the consequences of any delay or any loss arising as a result of being unable to contact the Client to complete the security checks.</p>
                            <p>14. The Company shall not be liable for any losses arising from delay in the transmission of the funds due to causes beyond its control. The Company shall not be held responsible for any losses or damages occurring as a result of military force, political intervention, the prescriptions of domestic or foreign authorities or events occurring as a result of catastrophe or Act of God.</p>
                            <p>15. The Company shall have the right at any time to amend the Service Agreement. Such amendments shall be notified to the Client by email and in the absence of any written objections shall be deemed to have been accepted after a period of one week has elapsed.</p>
                            <p>16. The contract between the Company and the Client, incorporating the above Service Agreement, shall be governed and construed in accordance with the laws of U.S. All parties to the contract agree to be bound by the non-exclusive jurisdiction of U.S. courts.</p>
                            <h3>Refund Policy</h3>
                            <p>All refund requests are honored and carefully investigated if made in a timely fashioned, and no more than 24 hours after the member's deposit transaction was completed.</p>
                            <h3>Copyright</h3>
                            <p>Bitbank-express is the owner or the licensee of all the intellectual property and rights of this website, including its entire published content. Unless otherwise noted, all materials including images, illustrations, designs, icons, photographs, written and other materials that appear as part of this website are copyrights, trademarks and/or other intellectual properties owned, controlled or licensed by Bitbank-express.</p>
                            <p>The website as a whole is protected by copyrights and all rights are reserved. The contents of our website, and the website as a whole, are intended solely for the use of our website's visitors. You may download or copy the contents and other downloadable materials displayed on the site for your personal use only. No right, title or interest in any downloaded materials is transferred to you as a result of any such downloading or copying. You may not reproduce (except as noted above), publish, transmit, distribute, display, modify, create derivative works from, sell or participate in any sale of, or exploit in any way, in whole or in part, any of the contents of this site.</p>
                            <p>The contents on this website may be used only as a service resource. Any other use, including reproduction, modification, distribution, transmission, republication, display or performance of the contents is strictly prohibited without the prior written consent of Bitbank-express, Cyprus. Nothing on this website should be construed as granting any license or right in relation to any of Bitbank-express' trademarks or those of their affiliated companies or any third party.</p>
                            <h3>Anti-Money Laundering Policy</h3>
                            <p>Bitbank-express has a zero-tolerance policy towards money laundering activities. Therefore, suspicious activities are monitored and reported immediately to the authorities.</p>
                            <p>We reserve the right to verify the identity of every customer that holds an account with Bitbank-express.</p>
                            <p>We will not transfer funds to third parties. Such requests will be granted only under specific circumstances.</p>
                            <p>Bitbank-express is not a bank; you may use our transfer facility only to fund your account and withdraw proceeds of your investments. Frequent transfer activity will be monitored and if Bitbank-express will detect that the account is used for anything other than investments, the account will be blocked and thorough documentation will be requested as to the origin of the funds and the identity of the beneficiaries.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

