<header id="fh5co-header" role="banner">
    <div class="container">
        <div class="header-inner">
            <h1><a href="{{ route('front.home') }}">BitBank<span>.</span></a></h1>
            <nav role="navigation">
                <ul>
                    <li><a href="{{ route('front.home') }}">Home</a></li>
                    <li><a href="{{ route('front.investment') }}">Investment</a></li>
                    <li><a href="{{ route('front.about') }}">About Us</a></li>
                    <li><a href="{{ route('front.faq') }}">FAQ</a></li>
                    <li><a href="{{ route('front.contact') }}">Contact</a></li>
                    @guest
                        <li class="cta"><a href="{{ route('auth.login.form') }}" id="loginform">Login</a></li>
                    @endguest
                    @auth
                        <li class="cta"><a href="{{ route('dashboard.home') }}">DashBoard</a></li>
                    @endauth
                </ul>
            </nav>
        </div>
    </div>
</header>