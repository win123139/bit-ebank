<footer id="fh5co-footer" role="contentinfo">
    <div class="container">
        <div class="col-md-3 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0">
            <h3>About us</h3>
            <p>Founded in 09/2008, up-to-date, we always take the lead in trading and storing Digital currencies.
                Definitely,We will bring satisfactions to you</p>
            <p><a href="{{ route('front.investment') }}" class="btn btn-primary btn-outline with-arrow btn-sm">Make
                    Deposit NOW !<i class="icon-arrow-right"></i></a></p>
        </div>
        <div class="col-md-6 col-md-push-1 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0">
            <h3>Our Services</h3>
            <ul class="float">
                <li><a href="#">Buying and Selling, Trading Digital currencies</a></li>
                <li><a href="#">Transaction & Conversion services</a></li>
                <li><a href="#">Providing Storing place for your Digital money</a></li>
                <li><a href="#">Predicting of the Changing in Currencies value</a></li>
            </ul>
        </div>
        <div class="col-md-2 col-md-push-1 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0">
            <h3>Follow Us</h3>
            <ul class="fh5co-social">
                <li><a href="#"><i class="icon-twitter"></i></a></li>
                <li><a href="#"><i class="icon-facebook"></i></a></li>
                <li><a href="#"><i class="icon-google-plus"></i></a></li>
                <li><a href="#"><i class="icon-instagram"></i></a></li>
            </ul>
        </div>
        <div class="col-md-12 fh5co-copyright text-center">
            <p>&copy; 2015 BITBANK - SINGAPORE NATIONAL BANK. All Rights Reserved.</p>
        </div>
    </div>
</footer>