@extends('front.layout')
@section('title','Bitbank Express')
@section('main-content')
    <aside id="fh5co-hero" class="js-fullheight">
        <div class="flexslider js-fullheight">
            <ul class="slides">
                <li style="background-image: url({{ asset('images/slide_3.jpg') }});">
                    <div class="overlay-gradient"></div>
                    <div class="container">
                        <div class="col-md-10 col-md-offset-1 text-center js-fullheight slider-text">
                            <div class="slider-text-inner">
                                <h2>Fastest and Safest investment. All your information are scured by us.</h2>
                                <p class="fh5co-lead">We guarante for it<i class="icon-heart"></i></p>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </aside>

    <div class="fh5co-about animate-box">

        <div class="container">
            <div class="col-md-8 col-md-push-4">
                <h2>A Membership from SINGAPORE NATIONAL BANK</h2>
                <p>Founded in 09/2008. Singapore, from a small team with Enthusiastic people. Passing through many
                    difficulties, in the beginning of Digital Currency's Age. We became stronger, more powerful in
                    financial ability. In 2016, We pleased to introduce you our NEW Website for exchange Digital
                    currencies and Plan for investment by YOUR-OWN. We empowering to you, hopefully in making your life
                    become easier with your profit earning from our services.</p>
            </div>
        </div>
    </div>

    <div id="fh5co-why-us" class="animate-box" style="padding: 50px;">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 text-center fh5co-heading">
                    <h2>Why Choose Us</h2>
                    <p>SAFE - STABLE - SECURE - That values we will take to you</p>
                </div>
                <div class="col-md-4 text-center item-block">
                    <span class="icon"><img src="{{ asset('images/30.svg') }}" alt="Free HTML5 Templates"
                                            class="img-responsive"></span>
                    <h3>Strategy</h3>
                    <p>With our plan, your investment will increase hourly, 24h in a day, 30 days in a month and 12
                        months in a year.</p>
                    <p><a href="#" class="btn btn-primary btn-outline with-arrow">Learn more <i
                                    class="icon-arrow-right"></i></a></p>
                </div>
                <div class="col-md-4 text-center item-block">
                    <span class="icon"><img src="{{ asset('images/18.svg') }}" alt="Free HTML5 Templates"
                                            class="img-responsive"></span>
                    <h3>Explore</h3>
                    <p>Your money and transactions will be kept in safe. By the most security system and best people in
                        IT field.</p>
                    <p><a href="#" class="btn btn-primary btn-outline with-arrow">Learn more <i
                                    class="icon-arrow-right"></i></a></p>
                </div>
                <div class="col-md-4 text-center item-block">
                    <span class="icon"><img src="{{ asset('images/27.svg') }}" alt="Free HTML5 Templates"
                                            class="img-responsive"></span>
                    <h3>Expertise</h3>
                    <p>The highest interest rates, exchange rates and many deposit plans. Your profit is up to you.</p>
                    <p><a href="#" class="btn btn-primary btn-outline with-arrow">Learn more <i
                                    class="icon-arrow-right"></i></a></p>
                </div>
            </div>
        </div>
    </div>

    <div class="fh5co-cta" style="background-image: url({{ asset('images/image_3.jpg') }});">
        <div class="overlay"></div>
        <div class="container">
            <div class="col-md-12 text-center animate-box">
                <h3>We Try To Update The Site Everyday</h3>
                @guest
                <p><a href="{{ route('auth.register.form') }}" class="btn btn-primary btn-outline with-arrow">Get
                        started now! <i class="icon-arrow-right"></i></a></p>
                @endguest
            </div>
        </div>
    </div>
@endsection