@extends('front.layout')
@section('title','Bitbank Express')
@section('main-content')
    <aside id="fh5co-hero" class="js-fullheight">
        <div class="flexslider js-fullheight">
            <ul class="slides">
                <li style="background-image: url({{ asset('images/slide_3.jpg') }});">
                    <div class="overlay-gradient"></div>
                    <div class="container">
                        <div class="col-md-10 col-md-offset-1 text-center js-fullheight slider-text">
                            <div class="slider-text-inner">
                                <h2>Make deposit and be rich with us. We will make you happy</h2>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </aside>

    <div id="tems_use" class="fh5co-contact animate-box">
        <div class="container">
            <div class="row">
                <div  class="col-md-8 col-md-push-1 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0" style="left: 15%;" >
                    <div class="row">
                        <h2 style="margin-left: 47%;">FAQ</h2>
                        <div class="col-md-12" style="margin-left: 15%;">
                            <table style="width: 80%; cellpadding: 10px;">
                                <tr>
                                    <th><a href="#General">General</a></th>
                                    <th><a href="#Investment">Investment</a></th>
                                    <th><a href="#Withdraws">Withdraws</a></th>
                                    <th><a href="#Affiliates">Affiliates</a></th>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <br />
                    <h3 id="General">General</h3>
                    <p><strong>1. I Would Like To Invest In Your Project, Is There Risk Factor Involved?</strong></p>
                    <p>Thanks to our engineers, financial experts and marketing team, Bitbank-Express Limited is a risk-free online investment platform that has already proven itself time and time again. We have a team of experienced cryptocurrency enthusiasts that makes cryptocurrency mining accessible to everyone. Through the years, we have built solid relationships with our partners all over the globe, which gives us the ability to perform lucrative Bitcoin cloud mining and provide earning opportunities to others.</p>
                    <p><strong>2. What’s Your Main Activity To Generate Such A High Interest?</strong></p>
                    <p>In addition to Bitcoin mining, we are also engaged in Bitcoin market trading. We have managed to monetize the marketplace volatility, and thanks to our experienced and talented investors, we are able to offer a more comprehensive array of services to our clients. As a trusted provider of Bitcoin mining, we are continuously working to improve our services through automated payment processing and an enhanced infrastructure. To prevent unnecessary financial losses, Bitbank-Express Limited only offers Bitcoin investments, and our track record to date has made us a preferred provider of investment potential for Bitcoins. If you’re looking to safeguard your financial future, Bitbank-Express Limited is the ideal choice.</p>
                    <p><strong>3. Can Anyone From Any Part Of The World Become An Official Member Of Your Company?</strong></p>
                    <p>There is no restriction policy applied to membership when it comes to citizenship, geographical location our any other outside related factor. Any participant from any part of the world is welcome to join, to benefit from Bitcoin cloud mining services that we provide as well as use our other available services.</p>
                    <p><strong>4. I Would Like To Register More Than One Account, Is This Allowed?</strong></p>
                    <p>No, this is not allowed. An individual is not allowed to own more than one account with our company to prevent any internal affiliate commission losses. However, sharing a single IP address with other users is allowed. Each account should contain legitimate and authentic personal information.</p>
                    <br/><br/>
                    <h3 id="Investment">Investment</h3>
                    <p><strong>1. What Payment Methods Are Available For Using Your Services?</strong></p>
                    <p>Currently we accept Bitcoin crypto-currency as the only payment method.</p>
                    <p><strong>2. Are There Any Fees For Transactions? If So, What Are They?</strong></p>
                    <p>There are no internal fees within Bitbank-Express for both your investments and withdrawals. A 5% fee is applied for initial deposit withdrawal which is possible any time after a minimum of 24 hours since you made a deposit.</p>
                    <p><strong>3. After I Have Made A Deposit, How Long Should I Wait For It To Be Added To My Account?</strong></p>
                    <p>Bitcoin investments usually are added within 30 minutes after sending the funds (due to Bitcoin network confirmations required).</p>
                    <p><strong>4. I Have An Active Deposit, Can I Add Additional Funds To It?</strong></p>
                    <p>No, adding funds to an existing deposit is not technically possible. Once you’ve made a deposit, the amount is final and cannot be changed, however there are no restrictions on how many deposits you can have.</p>
                    <p><strong>5. How Many Deposits Can I Have?</strong></p>
                    <p>You can have as many deposits as you want to, there are no restrictions</p>
                    <br/><br/>
                    <h3 id="Withdraws">Withdraws</h3>
                    <p><strong>1. What’s The Withdrawal Processing Time After I Have Requested It?</strong></p>
                    <p>All withdrawals are processed instantly, however sometimes it can take up to 24 hours because of security reasons, or while we are performing server or system upgrades. You do not have to contact support regarding your withdrawal request; it will be processed in case it was not processed instantly, within 24 business hours.</p>
                    <p><strong>2. What’s The Minimum And Maximum Withdrawal Amount?</strong></p>
                    <p>The minimum withdrawal amount is 0.0001 BTC and there is no maximum limit. You can request as many withdrawals per day as you wish.</p>
                    <p><strong>3. Is It Possible To Deposit Money Via Bitcoin And Withdraw Via Other Cryptocurrency?</strong></p>
                    <p>No, it's not possible. We do not provide cryptocurrency exchange services.</p>
                    <p><strong>4. When Will I Be Able To Receive Back My Principal Investment?</strong></p>
                    <p>The principal (your initial investment amount) can be released at any time (after a minimum of 24 hours since you made a deposit) in your members area. There's a 5% deposit cancellation fee applied.</p>
                    <br/><br/>
                    <h3 id="Affiliates">Affiliates</h3>
                    <p><strong>1. I Would Like To Earn More By Participating In Your Affiliate Program, How Does It Work?</strong></p>
                    <p>We have a structured a multi-level affiliate program to provide customers with an additional option to earn and profit from our company. In your account, in the referrals section, you will be able to find your affiliate link - simply share it with others. After another individual uses your affiliate link to register they automatically becomes your referral. From this point on, on all deposits that they make, you will earn a referral commission. For detailed affiliate payout structure please visit Partnership page.</p>
                    <p><strong>2. Do You Credit Referral Commissions For Deposits Made Using The Available Account Balance?</strong></p>
                    <p>No, referral commissions are only credited for deposits made directly using the payment method Bitcoin.</p>
                    <p><strong>3. Do I Have To Have An Active Deposit To Benefit From The Affiliate Program?</strong></p>
                    <p>No, you don’t need an active investment to earn affiliate commissions. Feel free to share your affiliate link with others, via any type of legal promotion.</p>
                    <p><strong>4. I Would Like To Become A Regional Representative, How Can I Become One?</strong></p>
                    <p>Regional Representatives earn twice as much referral commission from referrals they attract to our company, which means they are doubled for all 3 levels. In order to become our Representative you have to have the ability to support and promote our project in your region. You have to support people via telephone, online chats and offline meetings into your region. Use any legal promotional way possible to introduce people to our project, such as videos, blog articles, seminars, etc. In order to become a Representative all you have to do is email to contact@bitbank.finalcial, and you will receive a response within 24 business hours.</p>
                </div>
            </div>
        </div>
    </div>
@endsection