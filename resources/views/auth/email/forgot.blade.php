<table width="100%" border="0" cellspacing="0" cellpadding="17">
    <tbody>
    <tr>
        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td>
                        <div style="font-family:&quot;trebuchet ms&quot;;font-size:18px;line-height:20px;color:rgb(255,38,168);font-weight:bold">
                            Welcome to Bit-eBank!
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="font-family:&quot;trebuchet ms&quot;;font-size:18px;line-height:20px;color:rgb(255,38,168);font-weight:bold">
                            Dear {{ $user->firstname }},
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr></tr>
    <tr>
        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td valign="top">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                            <tr>
                                <td bgcolor="">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                        <tr>
                                            <td style="line-height:22px">
                                                <div style="color:rgb(59,106,121);font-family:&quot;trebuchet ms&quot;;font-size:17px;line-height:20px;font-weight:bold"></div>
                                                <p>Bit-Ebank– and you - are joining an exploding digital transformation
                                                    of
                                                    the global economy. This transformation will not merely rival all of
                                                    the
                                                    great financial transformations in history; it will re-define the
                                                    way
                                                    ALL people on Earth do business!<br>And to repeat, we (you) are just
                                                    getting started!<br>Now for some business….</p>
                                                <p style="color:rgb(116,116,116);font-family:arial;font-size:14px">
                                                    <strong>Your Link Reset Password: </strong>
                                                </p>
                                                <p>{{ route('auth.forgot.verify',[
                                                        'token' => $user->token
                                                    ]) }}</p>
                                                <p style="color:rgb(116,116,116);font-family:arial;font-size:14px">This
                                                    information is very important because you will be using it as part
                                                    of your login. Please change the password after login
                                                    successfully</p>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>