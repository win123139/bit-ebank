<table width="100%" border="0" cellspacing="0" cellpadding="17">
    <tbody>
    <tr>
        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td>
                        <div style="font-family:&quot;trebuchet ms&quot;;font-size:18px;line-height:20px;color:rgb(255,38,168);font-weight:bold">
                            Welcome to Bit-eBank!
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="font-family:&quot;trebuchet ms&quot;;font-size:18px;line-height:20px;color:rgb(255,38,168);font-weight:bold">
                            Dear , {{ $user->getFullName() }}
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr></tr>
    <tr>
        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td valign="top">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                            <tr>
                                <td bgcolor="">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                        <tr>
                                            <td style="line-height:22px">
                                                <div style="color:rgb(59,106,121);font-family:&quot;trebuchet ms&quot;;font-size:17px;line-height:20px;font-weight:bold"></div>
                                                <p style="color:rgb(116,116,116);font-family:arial;font-size:14px">
                                                    First, in the three years of our existence, Bit-eBank has already
                                                    risen into the top twenty of all worldwide cryptocurrencies in terms
                                                    of market cap and free-market value. Second, we are being promoted
                                                    by an exploding community of miners, customers and affiliates. And
                                                    it’s just getting started!<br>And so are you! Whether you are a
                                                    customer, miner or affiliate (or ALL three), you’re at the
                                                    proverbial ground-floor! Congratulations!<br>Bit-Ebank– and you -
                                                    are joining an exploding digital transformation of the global
                                                    economy. This transformation will not merely rival all of the great
                                                    financial transformations in history; it will re-define the way ALL
                                                    people on Earth do business!<br>And to repeat, we (you) are just
                                                    getting started!<br>Now for some business….</p>
                                                <p style="color:rgb(116,116,116);font-family:arial;font-size:14px">Your
                                                    login email is:&nbsp;<a href=""
                                                                            style="font-family:sans-serif;font-size:13.12px;background-color:rgb(255,255,255)"
                                                                            target="_blank">{{ $user->email }}</a></p>
                                                <p style="color:rgb(116,116,116);font-family:arial;font-size:14px">Your
                                                    password is: {{ $password }}</p>
                                                <p style="color:rgb(116,116,116);font-family:arial;font-size:14px">Your
                                                    CCno: {{ $user->ccno }}</p>
                                                <p style="color:rgb(116,116,116);font-family:arial;font-size:14px">Your
                                                    Account: {{ $user->accountno }}</p>
                                                <p style="color:rgb(116,116,116);font-family:arial;font-size:14px">This
                                                    information is very important because you will be using it as part
                                                    of your login credentials to access your account.</p>
                                                <p>
                                                    <span style="color:rgb(80,107,130);font-family:arial;font-size:14px;font-weight:bold">Login address:&nbsp;</span><font
                                                            color="#506b82" face="arial"><span
                                                                style="font-size:14px"><b><a
                                                                        href="{{ route('auth.login.form') }}"
                                                                        target="_blank"
                                                                        data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=http://bit-ebank.com/index/uslogin.php&amp;source=gmail&amp;ust=1492478987348000&amp;usg=AFQjCNFgtztpVKeLzSqJQhKWv6kYQwsjGQ">{{ route('auth.login.form') }}</a></b></span></font>
                                                </p>
                                                <p style="color:rgb(116,116,116);font-family:arial;font-size:14px">
                                                    Finally. if you haven’t already, please take a few minutes for
                                                    changing your password. We will be adding new material constantly.
                                                    Stay informed.</p>
                                                <p style="color:rgb(116,116,116);font-family:arial;font-size:14px">
                                                    Again, welcome to&nbsp;<span class="m_451347967237305246gmail-il">Bit-eBank</span>!
                                                </p></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>