@extends('auth.layout')
@section('title','Bitbank | User Login')

@section('style')
    <link href="{{ asset('assets/pages/css/login.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('body-class','login')
@section('body-bg',asset('images/singapore_bg.jpg'))
@section('content')
    <h3 class="form-title font-green">Hi, {{ $user->getFullName() }}</h3>
    <p> Please change the password after login successfully  </p>
    <p> Your password is :  {{ $newPassword }} </p>
    <div class="form-actions">
        <a class="btn green btn-outline" id="btn_back" href="{{ route('auth.login.form') }}">Back</a>
    </div>
@endsection