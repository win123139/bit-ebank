@extends('auth.layout')
@section('title','Bitbank | User Login')

@section('style')
    <link href="{{ asset('assets/pages/css/login.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('body-class','login')
@section('body-bg',asset('images/singapore_bg.jpg'))
@section('content')
    <!-- BEGIN LOGIN FORM -->
    <form class="login-form" action="{{ route('auth.login.post') }}" method="post">
        {{ csrf_field()  }}
        <h3 class="form-title font-green">Sign In</h3>
        @if ($errors->any())
            <div class="alert alert-danger" style="margin-left: 1%;">
                <p style="color: red;">Something went wrong!!</p>
            </div>
        @endif
        @if(Session::has('status'))
            <div class="alert alert-{{ Session::get('status') }}" style="margin-left: 1%;">
                <p>{{ Session::get('message') }}</p>
            </div>
        @endif
        <div class="alert alert-danger display-hide">
            <button class="close" data-close="alert"></button>
            <span> Enter any username and password. </span>
        </div>
        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
            <label class="control-label visible-ie8 visible-ie9">Email</label>
            <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off"
                   placeholder="Email" name="email" value="{{ Session::has('email') ? Session::get('email') : old('email') }}"/></div>
        <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
            <label class="control-label visible-ie8 visible-ie9">Password</label>
            <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off"
                   placeholder="Password" name="password"/></div>
        <div class="form-group {{ $errors->has('ccno') ? ' has-error' : '' }}">
            <label class="control-label visible-ie8 visible-ie9">CCno.</label>
            <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off"
                   placeholder="CCno." name="ccno" value="{{ Session::has('ccno') ? Session::get('ccno') :old('ccno') }}"/></div>
        <div class="form-actions">
            <a class="btn green btn-outline" id="btn_back" href="{{ route('front.home') }}">Back</a>
            <button type="submit" class="btn green uppercase pull-right" name="btn_submit" id="btn_submit">Login
            </button>
        </div>
        <a href="{{ route('auth.forgot') }}" id="forget-password" class="forget-password">Forgot Password?</a>

    </form>
    <!-- END LOGIN FORM -->
@endsection