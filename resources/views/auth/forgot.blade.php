@extends('auth.layout')
@section('title','Bitbank | User Login')

@section('style')
    <link href="{{ asset('assets/pages/css/login.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('body-class','login')
@section('body-bg',asset('images/singapore_bg.jpg'))
@section('content')
    <form id="login" action="{{ route('auth.forgot.send') }}" method="POST" novalidate>
        {{ csrf_field() }}
        @if(Session::has('status'))
            <div class="col-md-12 alert alert-{{ Session::get('status') }}" style="margin-left: 1%;">
                <p>{{ Session::get('message') }}</p>
            </div>
        @endif
        <h3 class="form-title font-green">Forget Password ?</h3>
        <p> Enter your e-mail address below to reset your password. </p>
        <div class="form-group">
            <input class="form-control" id="email" name="email" required placeholder="Your email" type="email">
        </div>
        <div class="form-actions">
            <a class="btn green btn-outline" id="btn_back" href="{{ route('auth.login.form') }}">Back</a>
            <button type="submit" class="btn green uppercase pull-right" name="btn_submit" id="btn_submit">Submit
            </button>
        </div>
    </form>
@endsection