<div class="page-sidebar-wrapper">
    <!-- BEGIN SIDEBAR -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <ul class="page-sidebar-menu  page-header-fixed" data-keep-expanded="false" data-auto-scroll="true"
            data-slide-speed="200" style="padding-top: 20px">
            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
            <li class="sidebar-toggler-wrapper hide">
                <div class="sidebar-toggler">
                    <span></span>
                </div>
            </li>
            <!-- END SIDEBAR TOGGLER BUTTON -->
            <li class="nav-item {{ Request::is('dashboard') ? 'open active' : '' }}">
                <a href="{{ route('dashboard.home') }}" class="nav-link nav-toggle">
                    <i class="icon-home"></i>
                    <span class="title" href="personal.php">Dashboard</span>
                    @if(Request::is('dashboard'))
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    @else
                        <span class="arrow"></span>
                    @endif
                </a>

            </li>
            <li class="heading">
                <h3 class="uppercase">Features</h3>
            </li>
            <li class="nav-item {{ Request::is('dashboard/wallet') ? 'open active' : '' }}">
                <a href="{{ route('dashboard.wallet') }}" class="nav-link nav-toggle">
                    <i class="icon-wallet"></i>
                    <span class="title">Wallet</span>
                    @if(Request::is('dashboard/wallet'))
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    @else
                        <span class="arrow"></span>
                    @endif
                </a>
            </li>
            <li class="nav-item {{ Request::is('dashboard/referrals') ? 'open active' : '' }}">
                <a href="{{ route('dashboard.referrals') }}" class="nav-link nav-toggle">
                    <i class="icon-globe"></i>
                    <span class="title">Direct Referrals</span>
                    @if(Request::is('dashboard/referrals'))
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    @else
                        <span class="arrow"></span>
                    @endif
                </a>
            </li>
            <li class="nav-item  {{ Request::is('dashboard/binary') ? 'open active' : '' }}">
                <a href="{{ route('dashboard.binary') }}" class="nav-link nav-toggle">
                    <i class="icon-layers"></i>
                    <span class="title">Binary Tree</span>
                    @if(Request::is('dashboard/binary'))
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    @else
                        <span class="arrow"></span>
                    @endif
                </a>
            </li>
            <li class="nav-item  {{ Request::is('dashboard/transactions') ? 'open active' : '' }}">
                <a href="{{ route('dashboard.transactions') }}" class="nav-link nav-toggle">
                    <i class="icon-folder"></i>
                    <span class="title">Transactions</span>
                    @if(Request::is('dashboard/transactions'))
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    @else
                        <span class="arrow"></span>
                    @endif
                </a>
            </li>
            <li class="nav-item  {{ Request::is('dashboard/deposit') ? 'open active' : '' }}">
                <a href="{{ route('dashboard.deposit') }}" class="nav-link nav-toggle">
                    <i class="icon-bar-chart"></i>
                    <span class="title">Deposit</span>
                    @if(Request::is('dashboard/deposit'))
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    @else
                        <span class="arrow"></span>
                    @endif
                </a>
            </li>
            <li class="nav-item {{ Request::is('dashboard/commission') ? 'open active' : '' }}">
                <a href="{{ route('dashboard.commission.index') }}" class="nav-link nav-toggle">
                    <i class="icon-briefcase"></i>
                    <span class="title">Commission</span>
                    @if(Request::is('dashboard/commission'))
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    @else
                        <span class="arrow"></span>
                    @endif
                </a>
            </li>
            <li class="heading">
                <h3 class="uppercase">Pages</h3>
            </li>

            <li class="nav-item {{ Request::is('dashboard/user*') ? 'open active' : '' }}">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-user"></i>
                    <span class="title">User</span>
                    @if(Request::is('dashboard/user*'))
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    @else
                        <span class="arrow"></span>
                    @endif
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  {{ Request::is('dashboard/user/overview') ? 'open active' : '' }}">
                        <a href="{{ route('dashboard.user.overview') }}" class="nav-link ">
                            <i class="icon-user"></i>
                            <span class="title">Overview</span>
                            @if(Request::is('dashboard/user/overview'))
                                <span class="selected"></span>
                                <span class="arrow open"></span>
                            @else
                                <span class="arrow"></span>
                            @endif
                        </a>
                    </li>
                    <li class="nav-item  {{ Request::is('dashboard/user/profile') ? 'open active' : '' }}">
                        <a href="{{ route('dashboard.user.profile') }}" class="nav-link ">
                            <i class="icon-user-female"></i>
                            <span class="title">Account Setting</span>
                            @if(Request::is('dashboard/user/profile'))
                                <span class="selected"></span>
                                <span class="arrow open"></span>
                            @else
                                <span class="arrow"></span>
                            @endif
                        </a>
                    </li>

                    <li class="nav-item  ">
                        <a href="{{ route('auth.logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"
                           class="nav-link nav-toggle">
                            <i class="icon-notebook"></i>
                            <span class="title">Logout</span>
                            <span class="arrow"></span>
                        </a>
                    </li>

                </ul>
            </li>
            <li class="nav-item  ">
                <a href="{{ route('dashboard.contact') }}" class="nav-link nav-toggle">
                    <i class="icon-social-dribbble"></i>
                    <span class="title">Contact</span>
                    <span class="arrow"></span>
                </a>

            </li>

        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>