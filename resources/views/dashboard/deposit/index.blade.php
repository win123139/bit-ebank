@extends('dashboard.layout')
@section('title','BitBank | Deposit')
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE TABLE PORTLET-->
                    <div class="portlet box green">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-cogs"></i>Select Plan
                            </div>

                        </div>
                        <div class="portlet-body flip-scroll">
                            @if (session('error'))
                            <div class="panel panel-warning">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Error</h3>
                                </div>
                                <div class="panel-body">{{ session('error') }}</div>
                            </div>
                            @endif
                            
                            @if (session('message'))
                            <div class="panel panel-success">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Success</h3>
                                </div>
                                <div class="panel-body">{{ session('message') }}</div>
                            </div>
                            @endif
                            <form action="{{ route('dashboard.deposit') }}" method="POST" class="form-horizontal form-bordered">
                                {{ csrf_field() }}
                                <div class="form-body">

                                    <div class="form-group">
                                        <label class="control-label col-md-3"><strong>Maximum Deposit</strong></label>
                                        <div class="col-md-4">
                                            <p name="available_balance" id="available_balance"
                                               style="padding-top: 8px;">{{ number_format($balance, 3) }}</p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3"><strong>Amount</strong></label>
                                        <div class="col-md-4">
                                            <input name="amount" type="number" step="0.001" min="0"
                                                   max="{{ $balance }}"
                                                   class="form-control" id="maxlength_defaultconfig" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3"><strong>Plan</strong></label>
                                        <div class="col-md-4">
                                            <div class="mt-radio-list">
                                                <label class="mt-radio">
                                                    <input type="radio" name="optionsRadios21" id="btc_1"
                                                           value="0" checked=""> 0.2 - 1 BTC | Monthly profit : 20%
                                                    <span></span>
                                                </label>
                                                <label class="mt-radio">
                                                    <input type="radio" name="optionsRadios21" id="btc_1"
                                                           value="1" checked=""> 1 - 3 BTC | Monthly profit : 20%
                                                    <span></span>
                                                </label>
                                                <label class="mt-radio">
                                                    <input type="radio" name="optionsRadios21" id="btc_2"
                                                           value="3" checked=""> 3 - 5 BTC | Monthly profit : 20%
                                                    <span></span>
                                                </label>
                                                <label class="mt-radio">
                                                    <input type="radio" name="optionsRadios21" id="btc_3"
                                                           value="5" checked=""> 5 - 7 BTC | Monthly profit : 20%
                                                    <span></span>
                                                </label>
                                                <label class="mt-radio">
                                                    <input type="radio" name="optionsRadios21" id="btc_4"
                                                           value="7" checked=""> 7 - 10 BTC | Monthly profit : 20%
                                                    <span></span>
                                                </label>
                                                <label class="mt-radio">
                                                    <input type="radio" name="optionsRadios21" id="btc_5"
                                                           value="10" checked=""> 10 BTC | Monthly profit : 20%
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button class="btn blue btn-circle" type="submit" value="submit">Submit
                                            </button>

                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>


                    </div>
                    <!-- END SAMPLE TABLE PORTLET-->
                    <div class="portlet-body" style="margin-bottom: 15px;">
                        <div class="row">
                            @if ($packages)
                            @foreach ($packages as $package)
                                <div class="col-md-4">
                                    <div class="mt-widget-3">
                                        <div class="mt-head bg-blue-hoki">
                                            <div class="mt-head-icon">
                                                <i class=" icon-user"></i>
                                            </div>
                                            <div class="mt-head-desc"> Profit <strong>20%</strong>
                                            </div>
                                            <span class="mt-head-date"> Start Date: <strong>{{ $package->start_date }}</strong></span>
                                            <span class="mt-head-date"> Monthly Payment: <strong>{{ $package->pay_at }}</strong></span>
                                            <div class="mt-head-button">
                                                <button type="button"
                                                        class="btn btn-circle btn-bitcoin btn-outline white btn-sm"
                                                        data-btc="btc_1">{{ $package->amount }} BTC
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            @endif
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $(document).on('click','.btn-bitcoin',function(){
                var $btn = $(this),
                    btc = $btn.data('btc');
                $('input#'+btc).prop('checked', true);
            });
        });
    </script>
@endsection