@extends('dashboard.layout')
@section('title','BitBank | Transactions')

@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE TABLE PORTLET-->
                    <div class="portlet box green">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-cogs"></i>Transaction History
                            </div>

                        </div>
                        <div class="portlet-body flip-scroll">
                            <table class="table table-bordered table-striped table-condensed flip-content">
                                <thead class="flip-content">
                                <tr>
                                    <th width="20%"> Date</th>
                                    <th class="numeric"> Amount</th>
                                    <th> Receive Wallet</th>
                                    <th> TX Hash</th>
                                    <th> Type</th>
                                    <th> Status</th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($transactions as $transaction)
                                    <tr>
                                        <td>{{ $transaction->getDate() }}</td>
                                        <td>{{ $transaction->amount }}</td>
                                        <td>{{ $transaction->receive_wallet }}</td>
                                        <td>{{ $transaction->transaction_id }}</td>
                                        <td>{{ $transaction->type }}</td>
                                        <td><span class="label label-sm label-{{ $transaction->getStatusView() }}">{{ $transaction->status }}</span></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div>
                                {{ $transactions->links() }}
                            </div>
                        </div>
                    </div>
                    <!-- END SAMPLE TABLE PORTLET-->
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
@endsection