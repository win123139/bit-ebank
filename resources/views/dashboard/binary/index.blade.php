@extends('dashboard.layout')
@section('title','BitBank | Binary')
@section('style')
    <link href="{{ asset('css/family_tree.css') }}" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet-body">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab_1_1" data-toggle="tab" aria-expanded="true"> Binary Tree </a>
                            </li>
                            <li class="">
                                <a href="#tab_1_2" data-toggle="tab" aria-expanded="false"> Summary </a>
                            </li>

                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="tab_1_1"
                                 style="width: 1100px; height: 600px; background-color: white;">

                                <div class="tree" style="overflow-x:scroll;">
                                    @include('dashboard.binary.recursive',compact('curUsers','level'))
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab_1_2">
                                <!-- BEGIN SAMPLE TABLE PORTLET-->
                                @php
                                    /** @var $curUser \App\Models\BinaryDownline */
                                    $curUser = $curUsers->first();
                                @endphp
                                <div class="col-md-6">
                                    <div class="portlet box green">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                LEFT
                                            </div>
                                            <div class="tools">
                                                <p>Total: {{ empty($curUser) ? 0 : $curUser->getLeftDeposit() }} BTC</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <!-- BEGIN CONDENSED TABLE PORTLET-->
                                    <div class="portlet box red">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="fa fa-picture"></i>RIGHT
                                            </div>
                                            <div class="tools">
                                                <p>Total: {{ empty($curUser) ? 0 : $curUser->getRightDeposit() }} BTC</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END CONDENSED TABLE PORTLET-->
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
@endsection