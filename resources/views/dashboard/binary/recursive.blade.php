@if ($level > 0)
    @php($level--)
    <ul>
        @foreach($curUsers as $key => $curUser)
            @if(is_null($curUsers->get($key)))
                <li>
                    <a href="{{ route('auth.register.form',['accountno' => $accountno, 'leg' => $key]) }}">Add new
                        member</a>
                </li>
            @else

                <li>
                    <a href="{{ route('dashboard.binary',['accountno' => $curUser->accountno]) }}">
                        <p><strong>{{ $curUser->name }}</strong></p>
                        {{ $curUser->accountno }}
                    </a>
                    @if ($level > 0)
                        @include('dashboard.binary.recursive',[
                            'curUsers'  => $curUser->getBinaryTree(),
                            'level'     => $level,
                            'accountno' => $curUser->accountno
                        ])
                    @endif
                </li>
            @endif
        @endforeach
    </ul>
@endif