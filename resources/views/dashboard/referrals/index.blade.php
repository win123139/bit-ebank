@extends('dashboard.layout')
@section('title','BitBank | Referrals')
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE TABLE PORTLET-->

                    <div class="portlet box green">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-cogs"></i>Direct Referrals
                            </div>

                        </div>

                        <div class="portlet-body flip-scroll">
                            <table class="table table-bordered table-striped table-condensed flip-content">
                                <thead class="flip-content">
                                <tr>
                                    <th class="numeric">#</th>
                                    <th width="20%"> Full Name</th>
                                    <th> Joined</th>
                                    <th class="numeric"> Deposit Amount</th>
                                    <th> Status</th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $index => $user)
                                    <tr>
                                        <td>{{ $index++ }}</td>
                                        <td>{{ $user->getFullName() }}</td>
                                        <td>{{ $user->created_date }}</td>
                                        <td>{{ $user->getDepositAmount() }}</td>
                                        <td>Active</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div>
                                {{ $users->links() }}
                            </div>
                        </div>
                    </div>
                    <!-- END SAMPLE TABLE PORTLET-->
                </div>
            </div>
        </div>
    </div>
@endsection