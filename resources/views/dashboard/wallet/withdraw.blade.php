@extends('dashboard.layout')
@section('title','BitBank | Wallet')
@section('style')

@endsection
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title" style=" margin-bottom: 0px;"> Wallet
                <small>Personal Wallet</small>
            </h1>
            <!-- END PAGE TITLE-->
            <!-- END PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <div>
                        <p>
                        <div id="id_qrcode" style="display: block;margin-left: 40%;">

                        </div>
                        </p>
                    </div>

                    <div class="portlet-body">
                        <h4><strong>Wallet Information</strong></h4>
                        <div class="margin-top-10 margin-bottom-10 clearfix">
                            <table class="table table-bordered table-striped">
                                <tr>
                                    <td><strong>Wallet Address</strong></td>
                                    <td>

                                        <strong>{{ $walletAddress }}</strong>

                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Balance</strong>
                                    </td>
                                    <td>
                                        <div id="pulsate-once-target" style="padding:5px;">{{ number_format($wallet_amount, 3)." BTC" }}</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>

                                    </td>
                                    <td>
                                        <form action="wallet.php" method="post">
                                            <button type="submit" value="Withdraw" class="btn blue btn-circle"
                                                    name="btn_withdraw">Withdraw
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <form action="{{ route('dashboard.withdraw') }}" method="post">
                        {!! csrf_field() !!}
                        <div class="note note-info">
                            <h4 class="block">We sent an email to your registered email. Please check it.</h4>
                            <input type="number" placeholder="Verify Code in email" class="form-control"
                                   name="verify_code" required><br/>
                            <input type="text" placeholder="CCno" class="form-control" name="ccno" required><br/>
                            <input type="number" step="0.001" placeholder="Number of BTC want to withdraw"
                                   class="form-control" name="moneyamount" id="moneyamount" required><br/>
                            <input type="text" placeholder="Wallet Address" class="form-control" name="receivewallet"
                                   id="receivewallet" required><br/>
                            <br/>
                            <button value="Withdraw" class="btn blue" name="btn_withdraw_do" id="btn_withdraw_do">
                                Withdraw
                            </button>
                        </div>
                    </form>

                    @if (session('error'))
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <h3 class="panel-title">Error</h3>
                        </div>
                        <div class="panel-body">{{ session('error') }}</div>
                    </div>
                    @endif
                    
                    @if (session('message'))
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">Withdraw Success</h3>
                        </div>
                        <div class="panel-body">{{ session('message') }}</div>
                    </div>
                    @endif

                </div>
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE TABLE PORTLET-->
                    <div class="portlet box green">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-cogs"></i>Topup History
                            </div>

                        </div>
                        <div class="portlet-body flip-scroll">
                            <table class="table table-bordered table-striped table-condensed flip-content">
                                <thead class="flip-content">
                                <tr>
                                    <th width="20%"> Date</th>
                                    <th class="numeric"> Amount</th>
                                    <th> Receive Wallet</th>
                                    <th> TX Hash</th>
                                    <th> Type</th>
                                    <th> Status</th>

                                </tr>
                                </thead>
                                <tbody>
                                @if($transactionHistory)
                                    @foreach ($transactionHistory as $item)
                                    <tr>
                                        <td>{{ date('d-m-Y',strtotime($item->date)) }}</td>
                                        <td>{{ $item->amount }}</td>
                                        <td>{{ $item->receive_wallet }}</td>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->type }}</td>
                                        @if($item->status == 'Pending')
                                        <td><span class="label label-sm label-warning">Pending</span></td>
                                        @elseif($item->status == 'Completed')
                                        <td><span class="label label-sm label-info">Completed</span></td>
                                        @else($item->status == 'Cancelled')
                                        <td><span class="label label-sm label-danger">Cancelled</span></td>
                                        @endif
                                    </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                            <div>
                                {{ $transactionHistory->links() }}
                            </div>
                        </div>
                    </div>
                    <!-- END SAMPLE TABLE PORTLET-->

                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
@endsection
@section('script')
    <script type="text/javascript" src="{{ asset('js/qrcode.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            new QRCode("id_qrcode", {
                text: "{{ $walletAddress }}",
                width:200,
                height:200,
                colorDark:"#000000",
                colorLight:"#ffffff",
                correctLevel:QRCode.CorrectLevel.H
            });
        });
    </script>
@endsection