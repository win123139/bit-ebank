@extends('dashboard.layout')
@section('title','BitBank | Wallet')
@section('style')

@endsection
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title" style=" margin-bottom: 0px;"> Wallet
                <small>Personal Wallet</small>
            </h1>
            <!-- END PAGE TITLE-->
            <!-- END PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <div>
                        <p>
                        <div id="id_qrcode" style="display: block;margin-left: 40%;">

                        </div>
                        </p>
                    </div>

                    <div class="portlet-body">
                        <h4><strong>Wallet Information</strong></h4>
                        <div class="margin-top-10 margin-bottom-10 clearfix">
                            <table class="table table-bordered table-striped">
                                <tr>
                                    <td><strong>Wallet Address</strong></td>
                                    <td>

                                        <strong>{{ $walletAddress }}</strong>

                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Balance</strong>
                                    </td>
                                    <td>
                                        <div id="pulsate-once-target" style="padding:5px;">{{ number_format($wallet_amount, 3)." BTC" }}</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>

                                    </td>
                                    <td>
                                        <form action="{{ route('dashboard.wallet') }}" method="post">
                                            {!! csrf_field() !!}
                                            <button type="submit" value="Withdraw" class="btn blue btn-circle"
                                                    name="btn_withdraw">Withdraw
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE TABLE PORTLET-->
                    <div class="portlet box green">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-cogs"></i>Topup History
                            </div>

                        </div>
                        <div class="portlet-body flip-scroll">
                            <table class="table table-bordered table-striped table-condensed flip-content">
                                <thead class="flip-content">
                                <tr>
                                    <th width="20%"> Date</th>
                                    <th class="numeric"> Amount</th>
                                    <th> Receive Wallet</th>
                                    <th> TX Hash</th>
                                    <th> Type</th>
                                    <th> Status</th>

                                </tr>
                                </thead>
                                <tbody>
                                @if($transactionHistory)
                                    @foreach ($transactionHistory as $item)
                                    <tr>
                                        <td>{{ date('d-m-Y',strtotime($item->date)) }}</td>
                                        <td>{{ $item->amount }}</td>
                                        <td>{{ $item->receive_wallet }}</td>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->type }}</td>
                                        @if($item->status == 'Pending')
                                        <td><span class="label label-sm label-warning">Pending</span></td>
                                        @elseif($item->status == 'Completed')
                                        <td><span class="label label-sm label-info">Completed</span></td>
                                        @else($item->status == 'Cancelled')
                                        <td><span class="label label-sm label-danger">Cancelled</span></td>
                                        @endif
                                    </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                            <div>
                                {{ $transactionHistory->links() }}
                            </div>
                        </div>
                    </div>
                    <!-- END SAMPLE TABLE PORTLET-->

                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
@endsection
@section('script')
    <script type="text/javascript" src="{{ asset('js/qrcode.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            new QRCode("id_qrcode", {
                text: "{{ $walletAddress }}",
                width:200,
                height:200,
                colorDark:"#000000",
                colorLight:"#ffffff",
                correctLevel:QRCode.CorrectLevel.H
            });
        });
    </script>
@endsection