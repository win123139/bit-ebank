@extends('dashboard.layout')
@section('title','BitBank | Dashboard')
@section('style')
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
@endsection
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->

            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title"> Dashboard

            </h1>
            <!-- END PAGE TITLE-->
            <!-- BEGIN DASHBOARD STATS 1-->
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <a class="dashboard-stat dashboard-stat-v2 blue" href="#">
                        <div class="visual">
                            <i class="fa fa-comments"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                <span data-counter="counterup" data-value="{{ $walletAmount }}">{{ $walletAmount }}</span> BTC
                            </div>
                            <div class="desc"> Amount Balance</div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <a class="dashboard-stat dashboard-stat-v2 red" href="#">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                <span data-counter="counterup"
                                      data-value="{{ $userProfit }}">
                                    {{ $userProfit }}
                                </span>
                                BTC
                            </div>
                            <div class="desc"> Total Profit</div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <a class="dashboard-stat dashboard-stat-v2 green" href="#">
                        <div class="visual">
                            <i class="fa fa-shopping-cart"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                <span data-counter="counterup"
                                      data-value="{{ $referrals->count() }}">{{ $referrals->count() }}</span>
                            </div>
                            <div class="desc">Direct Referrals</div>
                        </div>
                    </a>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <a class="dashboard-stat dashboard-stat-v2 purple" href="#">
                        <div class="visual">
                            <i class="fa fa-globe"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                <span>$</span><span data-counter="counterup" data-value="{{ $priceUsd }}">{{ $priceUsd }}</span>
                            </div>
                            <div class="desc">BTC Price</div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="clearfix"></div>
            <!-- END DASHBOARD STATS 1-->
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="portlet-body">
                        <div class="mt-element-list">
                            <div class="mt-list-head list-news ext-1 font-white bg-grey-gallery">
                                <div class="list-head-title-container">
                                    <h3 class="list-title">News List</h3>
                                </div>
                                <div class="list-count pull-right bg-red">5</div>
                            </div>
                            <div class="mt-list-container list-news ext-1">
                                <ul>
                                    <li class="mt-list-item">
                                        <div class="list-icon-container">
                                            <a href="javascript:;"><i class="fa fa-angle-right"></i></a>
                                        </div>
                                        <div class="list-thumb">
                                            <a href="javascript:;">
                                                <img alt=""
                                                     src="https://www.dbs.com/iwov-resources/images/newsroom/Belt%20and%20Road%20Forum.jpg">
                                            </a>
                                        </div>
                                        <div class="list-datetime bold uppercase font-red"> 15 May 2017</div>
                                        <div class="list-item-content">
                                            <h3 class="uppercase"><a target="_blank"
                                                                     href="https://www.dbs.com/newsroom/DBS_Bank_and_Agricultural_Bank_of_China_ink_MOU_at_the_OBOR_Summit">DBS
                                                    Bank and Agricultural Bank of China ink MOU at the “OBOR Summit”</a>
                                            </h3>
                                            <p>MOU enhances existing partnership in support of OBOR</p>
                                        </div>
                                    </li>
                                    <li class="mt-list-item">
                                        <div class="list-icon-container">
                                            <a href="javascript:;"><i class="fa fa-angle-right"></i></a>
                                        </div>
                                        <div class="list-thumb">
                                            <a href="javascript:;">
                                                <img alt=""
                                                     src="https://www.dbs.com/iwov-resources/images/newsroom/DHLExpress_man-and-lady..jpg">
                                            </a>
                                        </div>
                                        <div class="list-datetime bold uppercase font-red"> 09 May 2017</div>
                                        <div class="list-item-content">
                                            <h3 class="uppercase">
                                                <a target="_blank"
                                                   href="https://www.dbs.com/newsroom/DBS_first_Asian_bank_to_go_green_fully_with_DHL_Express">DBS
                                                    – first Asian bank to ‘go green’ fully with DHL Express</a>
                                            </h3>
                                            <p>Bank’s global international express shipments now carbon neutral CO2
                                                emissions from DBS shipments offset through reinvestments in climate
                                                protection projects</p>
                                        </div>
                                    </li>
                                    <li class="mt-list-item">
                                        <div class="list-icon-container">
                                            <a href="javascript:;"><i class="fa fa-angle-right"></i></a>
                                        </div>
                                        <div class="list-thumb">
                                            <a href="javascript:;">
                                                <img alt="" src="../assets/global/img/portfolio/600x600/12.jpg">
                                            </a>
                                        </div>
                                        <div class="list-datetime bold uppercase font-red"> 10 Feb 2017</div>
                                        <div class="list-item-content">
                                            <h3 class="uppercase">
                                                <a target="_blank"
                                                   href="https://www.dbs.com/newsroom/DBS_and_Xero_join_forces_to_revolutionise_how_SMEs_manage_capital">DBS
                                                    and Xero join forces to revolutionise how SMEs manage capital</a>
                                            </h3>
                                            <p>First-of-its-kind collaboration in Asia will provide SMEs with real-time
                                                access to their financial data and insights to make efficient business
                                                decisions</p>
                                        </div>
                                    </li>
                                    <li class="mt-list-item">
                                        <div class="list-icon-container">
                                            <a href="javascript:;"><i class="fa fa-angle-right"></i></a>
                                        </div>
                                        <div class="list-thumb">
                                            <a href="javascript:;">
                                                <img alt="" src="../assets/global/img/portfolio/600x600/10.jpg">
                                            </a>
                                        </div>
                                        <div class="list-datetime bold uppercase font-red"> 01 Feb 2017</div>
                                        <div class="list-item-content">
                                            <h3 class="uppercase">
                                                <a target="_blank"
                                                   href="https://www.dbs.com/newsroom/For_Li_Chun_2017_deposit_money_during_auspicious_timings_via_DBS_Paylah">For
                                                    Li Chun 2017, deposit money during auspicious timings via DBS
                                                    Paylah!</a>
                                            </h3>
                                            <p>For Li Chun 2017, customers can deposit money during lucky timings via
                                                DBS PayLah! or at over 350 DBS/POSB cash deposit machines islandwide</p>
                                        </div>
                                    </li>
                                    <li class="mt-list-item">
                                        <div class="list-icon-container">
                                            <a href="javascript:;"><i class="fa fa-angle-right"></i></a>
                                        </div>
                                        <div class="list-thumb">
                                            <a href="javascript:;">
                                                <img alt="" src="../assets/global/img/portfolio/600x600/08.jpg">
                                            </a>
                                        </div>
                                        <div class="list-datetime bold uppercase font-red"> 24 Jan 2017</div>
                                        <div class="list-item-content">
                                            <h3 class="uppercase">
                                                <a target="_blank"
                                                   href="https://www.dbs.com/newsroom/DBS_launches_first_of_its_kind_online_Letter_of_Credit_service">DBS
                                                    launches first-of-its-kind online Letter of Credit service</a>
                                            </h3>
                                            <p>SINGAPORE,24 January 2017 - DBS has launched a new service that allows
                                                SMEs to apply for a Letter of Credit (LC) online within five minutes and
                                                have it issued within two business days.</p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="portlet light portlet-fit bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class=" icon-layers font-green"></i>
                                <span class="caption-subject font-green bold uppercase">Photos</span>
                            </div>
                            <div class="portlet-body">
                                <div class="mt-element-card mt-element-overlay">
                                    <div class="row">
                                        <div class="w3-content w3-display-container">
                                            <iframe class="mySlides" width="100%" height="627px"
                                                    src="https://www.youtube.com/embed/jHJwHt0vQHE?autoplay=1"></iframe>
                                            <iframe class="mySlides" width="100%" height="627px"
                                                    src="https://www.youtube.com/embed/atWk-Prpy9w"></iframe>
                                            <button class="w3-button w3-black w3-display-left" onclick="plusDivs(-1)">
                                                &#10094;
                                            </button>
                                            <button class="w3-button w3-black w3-display-right" onclick="plusDivs(1)">
                                                &#10095;
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->

        </div>
        @endsection
        @section('script')
            <script>
                var slideIndex = 1;
                showDivs(slideIndex);

                function plusDivs(n) {
                    showDivs(slideIndex += n);
                }

                function showDivs(n) {
                    var i;
                    var x = document.getElementsByClassName("mySlides");
                    if (n > x.length) {
                        slideIndex = 1
                    }
                    if (n < 1) {
                        slideIndex = x.length
                    }
                    for (i = 0; i < x.length; i++) {
                        x[i].style.display = "none";
                    }
                    x[slideIndex - 1].style.display = "block";
                }
            </script>
@endsection