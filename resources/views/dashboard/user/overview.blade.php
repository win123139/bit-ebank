@extends('dashboard.layout')
@section('title','BitBank | Overview')
@section('style')

@endsection
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->


            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title"> Profile Overview

            </h1>
            <!-- END PAGE TITLE-->
            <!-- END PAGE HEADER-->
            <div class="invoice">
                <div class="row invoice-logo">
                    <div class="col-xs-6 invoice-logo-space">
                        <img src="{{ asset('assets/pages/media/invoice/walmart.png') }}" class="img-responsive" alt=""/></div>

                </div>
                <hr/>
                <div class="row">
                    <div class="col-xs-4">
                        <h3>Client:</h3>
                        <ul class="list-unstyled">
                            <li>{{ Auth::user()->getFullName() }}</li>

                        </ul>
                    </div>
                    <div class="col-xs-4">
                        <h3>About:</h3>
                        <ul class="list-unstyled">
                            <li><strong>CCno. : </strong>{{ Auth::user()->ccno }}</li>
                            <li><strong>Email : </strong>{{ Auth::user()->email }}</li>
                            <li><strong>National : </strong>{{ Auth::user()->country }}</li>
                            <li><strong>Birthday: </strong>{{ Auth::user()->birthday }}</li>
                            <li><strong>Sponsor : </strong>{{ Auth::user()->referral }}</li>
                            <li><strong>Joined Date: </strong>{{ Auth::user()->created_date }}</li>
                        </ul>
                    </div>
                    <div class="col-xs-4 invoice-payment">
                        <h3>Payment Details:</h3>
                        <ul class="list-unstyled">
                            <li>
                                <strong>Account Name:</strong> Personal Account
                            </li>
                            <li>
                                <strong>Account Number:</strong> BB1492143020 </li>
                            <li>
                                <strong>SWIFT code:</strong> 949ee09d838b18b4042880ae97c67bf8 </li>

                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-4">
                        <div class="well">
                            <address>
                                <strong>Bitbank</strong>
                                <!--<br/> 321-323 Orchard Rd-->
                                <br/> Sinapore, SGN 57600
                                <br/>
                                <!--<abbr title="Phone">P:</abbr> (+65) 215-215-428 </address>-->
                                <address>
                                    <strong>Full Name</strong>
                                    <br/>
                                    <a>{{ Auth::user()->getFullName() }}</a>
                                </address>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
@endsection