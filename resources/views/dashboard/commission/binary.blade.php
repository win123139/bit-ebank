@extends('dashboard.layout')
@section('title','BitBank | Commission')
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <div class="row">
                <div class="col-md-6">
                    <!-- BEGIN SAMPLE TABLE PORTLET-->
                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-bell-o"></i>Commission Balance
                            </div>

                        </div>
                        <div class="portlet-body">
                            <div class="table-scrollable">
                                <table class="table table-striped table-bordered table-advance table-hover">
                                    <thead>
                                    <tr>
                                        <th>
                                            <i class="fa fa-briefcase"></i> <strong>TYPE</strong></th>

                                        <th>
                                            <i class="fa fa-shopping-cart"></i><strong> TOTAL </strong></th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <form action="{{route('dashboard.commission.email')}}" method="post">
                                        {!! csrf_field() !!}
                                        <tr>
                                            <td class="highlight">
                                                <div class="info"></div>
                                                <a href="javascript:;"> Referrals Bonus </a>
                                            </td>

                                            <td>
                                                {{number_format($referral_profit, 2)}} USD
                                            </td>
                                            <td>
                                                <button name="btn_withdraw_referral" type="submit" value="btn_withdraw_referral"
                                                        class="btn btn-outline btn-circle green btn-sm purple"><i
                                                            class="fa fa-share"></i> Withdraw
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="highlight">
                                                <div class="success"></div>
                                                <a href="javascript:;"> Binary Tree Bonus </a>
                                            </td>

                                            <td>
                                                {{number_format($binary_profit, 2)}} USD
                                            </td>
                                            <td>

                                                <button name="btn_withdraw_binary" type="submit" value="btn_withdraw_binary"
                                                        class="btn btn-outline btn-circle green btn-sm purple"><i
                                                            class="fa fa-share"></i> Withdraw
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="highlight">
                                                <div class="warning"></div>
                                                <a href="javascript:;"> Monthly Profit </a>
                                            </td>
                                            <td>
                                                {{number_format($deposit_profit, 2)}} BTC
                                            </td>

                                            <td>
                                                <button name="btn_withdraw_profit" type="submit" value="btn_withdraw_profit"
                                                        class="btn btn-outline btn-circle green btn-sm purple"><i
                                                            class="fa fa-share"></i> Withdraw
                                                </button>
                                            </td>
                                        </tr>
                                    </form>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END SAMPLE TABLE PORTLET-->
                </div>
                <div class="col-md-6">
                    @if (session('error'))
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <h3 class="panel-title">Error</h3>
                        </div>
                        <div class="panel-body">{{ session('error') }}</div>
                    </div>
                    @endif
                    
                    @if (session('message'))
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">Withdraw Success</h3>
                        </div>
                        <div class="panel-body">{{ session('message') }}</div>
                    </div>
                    @endif
                    <form action="{{route('dashboard.binary.commission')}}" method="post">
                        {!! csrf_field() !!}
                        <div class="note note-info">
                            <h4 class="block"><strong>Withdraw Binary Tree Bonus</strong></h4>
                            <input type="number" placeholder="Verify Code in email" class="form-control" name="verify_code" required><br/>
                            <input type="text" placeholder="CCno" class="form-control" name="ccno" required><br/>
                            <input type="number" placeholder="Number of money want to withdraw" step="0.01" class="form-control" name="moneyamount" id="moneyamount" required onchange="myFunction()"><br/>
                            <h4 id="demo"></h4>
                            <input type="hidden" class="form-control" style="width: 20%;" name="bitexchanged" id="bitexchanged" required readonly>
                            <br/>
                            <button value="Withdraw" class="btn blue btn-circle" name="btn_withdraw_referral_do" id="btn_withdraw_referral_do">Withdraw</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
@endsection
@section('script')
    <script type="text/javascript">
    function onReady()
    {
        var qrcode = new QRCode("id_qrcode", {
            text:"33fTPJQLAGqXdNucmgUBzZJ8Cbd4dMG3fA",
            width:200,
            height:200,
            colorDark:"#000000",
            colorLight:"#ffffff",
            correctLevel:QRCode.CorrectLevel.H
        });
    }
    
    function myFunction() {
        var x = document.getElementById("moneyamount").value;
        var y = x/<?php echo $today_ratio?>;
        document.getElementById("demo").innerHTML ="You will get: " + y.toFixed(4) + " BTC";
        document.getElementById("bitexchanged").value = y.toFixed(4);
    }
    </script>
@endsection