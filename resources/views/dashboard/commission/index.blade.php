@extends('dashboard.layout')
@section('title','BitBank | Commission')
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <div class="row">
                <div class="col-md-6">
                    <!-- BEGIN SAMPLE TABLE PORTLET-->
                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-bell-o"></i>Commission Balance
                            </div>

                        </div>
                        <div class="portlet-body">
                            <div class="table-scrollable">
                                <table class="table table-striped table-bordered table-advance table-hover">
                                    <thead>
                                    <tr>
                                        <th>
                                            <i class="fa fa-briefcase"></i> <strong>TYPE</strong></th>

                                        <th>
                                            <i class="fa fa-shopping-cart"></i><strong> TOTAL </strong></th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <form action="{{route('dashboard.commission.email')}}" method="post">
                                        {!! csrf_field() !!}
                                        <tr>
                                            <td class="highlight">
                                                <div class="info"></div>
                                                <a href="javascript:;"> Referrals Bonus </a>
                                            </td>

                                            <td>
                                                {{number_format($referral_profit, 2)}} USD
                                            </td>
                                            <td>
                                                <button name="btn_withdraw_referral" type="submit" value="btn_withdraw_referral"
                                                        class="btn btn-outline btn-circle green btn-sm purple"><i
                                                            class="fa fa-share"></i> Withdraw
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="highlight">
                                                <div class="success"></div>
                                                <a href="javascript:;"> Binary Tree Bonus </a>
                                            </td>

                                            <td>
                                                {{number_format($binary_profit, 2)}} USD
                                            </td>
                                            <td>

                                                <button name="btn_withdraw_binary" type="submit" value="btn_withdraw_binary"
                                                        class="btn btn-outline btn-circle green btn-sm purple"><i
                                                            class="fa fa-share"></i> Withdraw
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="highlight">
                                                <div class="warning"></div>
                                                <a href="javascript:;"> Monthly Profit </a>
                                            </td>
                                            <td>
                                                {{number_format($deposit_profit, 2)}} BTC
                                            </td>

                                            <td>
                                                <button name="btn_withdraw_profit" type="submit" value="btn_withdraw_profit"
                                                        class="btn btn-outline btn-circle green btn-sm purple"><i
                                                            class="fa fa-share"></i> Withdraw
                                                </button>
                                            </td>
                                        </tr>
                                    </form>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END SAMPLE TABLE PORTLET-->
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
@endsection