@extends('dashboard.layout')
@section('title','BitBank | Contact')
@section('style')
@endsection
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->

            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title"> Contact Us

            </h1>
            <!-- END PAGE TITLE-->
            <!-- END PAGE HEADER-->

            <div class="c-content-feedback-1 c-option-1">
                <div class="row">

                    <div class="col-md-6">
                        <div class="c-contact">
                            <div class="c-content-title-1">
                                <h3 class="uppercase">Keep in touch</h3>
                                <div class="c-line-left bg-dark"></div>
                                <p class="c-font-lowercase">Our helpline is always open to receive any inquiry or
                                    feedback. Please feel free to drop us an email from the form below and we will get
                                    back to you as soon as we can.</p>
                            </div>
                            @if($errors->any())
                                <div class="col-md-12 alert alert-danger">
                                    <p>Something went wrong!!!!</p>
                                </div>
                            @endif
                            @if(Session::has('status'))
                                <div class="col-md-12 alert alert-{{ Session::get('status') }}" style="margin-left: 1%;">
                                    <p>{{ Session::get('message') }}</p>
                                </div>
                            @endif
                            <form action="{{ route('dashboard.contact.send') }}" method="post" novalidate>
                                {{ csrf_field() }}
                                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                    <input type="text" name="name" placeholder="Your Name"
                                           class="form-control input-md" value="{{ old('name') }}">
                                </div>
                                <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                    <input type="text" name="email" placeholder="Your Email"
                                           class="form-control input-md" value="{{ old('email') }}">
                                </div>
                                <div class="form-group {{ $errors->has('ccno') ? 'has-error' : '' }}">
                                    <input type="text" name="ccno" placeholder="CCNo." class="form-control input-md" value="{{ old('ccno') }}">
                                </div>
                                <div class="form-group {{ $errors->has('message') ? 'has-error' : '' }}">
                                    <textarea rows="8" name="message" placeholder="Write comment here ..."
                                              class="form-control input-md">{{ old('message') }}</textarea>
                                </div>
                                <button type="submit" class="btn grey">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
@endsection