<table width="100%" border="0" cellspacing="0" cellpadding="17">
    <tbody>
    <tr>
        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td>
                        <div style="font-family:&quot;trebuchet ms&quot;;font-size:18px;line-height:20px;color:rgb(255,38,168);font-weight:bold">
                            Contact Form Dash Board
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="font-family:&quot;trebuchet ms&quot;;font-size:18px;line-height:20px;color:rgb(255,38,168);font-weight:bold">
                            From {{ $contactEmail->get('name') }} - {{ $contactEmail->get('email') }}
                            @if (!is_null($contactEmail->get('ccno')))
                                - {{ $contactEmail->get('ccno') }}
                            @endif
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr></tr>
    <tr>
        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td valign="top">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                            <tr>
                                <td bgcolor="">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                        <tr>
                                            <td style="line-height:22px">
                                                <div style="color:rgb(59,106,121);font-family:&quot;trebuchet ms&quot;;font-size:17px;line-height:20px;font-weight:bold"></div>
                                                <p>Message : </p>
                                                <p style="color:rgb(116,116,116);font-family:arial;font-size:14px">
                                                    {{ $contactEmail->get('message') }}
                                                </p>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>