<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/**
 * AUTHENTICATION
 */
Route::group(['prefix' => '/', 'as' => 'auth.'], function () {

    /**
     * Middleware Guest
     */
    Route::middleware(['guest'])->group(function () {
        // Login
        Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login.form');
        Route::post('/login', 'Auth\LoginController@login')->name('login.post');

        // Forgot
        Route::get('/forgot', 'Auth\ForgotPasswordController@index')->name('forgot');
        Route::post('/forgot', 'Auth\ForgotPasswordController@sendEmail')->name('forgot.send');
        Route::get('/forgot/verify/{token}', 'Auth\ForgotPasswordController@verifyMail')->name('forgot.verify');
    });
    Route::middleware(['auth'])->group(function () {
        Route::get('/register/{accountno?}/{leg?}', 'Auth\RegisterController@showRegistrationForm')->name('register.form');
        Route::post('/register', 'Auth\RegisterController@register')->name('register.post');
    });

    // Logout
    Route::post('/logout', 'Auth\LoginController@logout')->name('logout')->middleware('auth');
});


/**
 * FRONT PAGE
 */
Route::group(['prefix' => '/', 'as' => 'front.'], function () {

    // Home
    Route::get('/', function () {
        return view('front.home.index');
    })->name('home');

    // Investment
    Route::get('/investment', function () {
        return view('front.investment.index');
    })->name('investment');

    // About
    Route::get('/about', function () {
        return view('front.about.index');
    })->name('about');

    // Faq
    Route::get('/faq', function () {
        return view('front.faq.index');
    })->name('faq');

    // Contact
    Route::get('/contact', function () {
        return view('front.contact.index');
    })->name('contact');

    // Tems
    Route::get('/tems', function () {
        return view('front.tems.index');
    })->name('tems');
});

/**
 * Auth Middleware Group
 */
Route::middleware(['auth'])->group(function () {
    /**
     * DASHBOARD PAGE
     */
    Route::group(['prefix' => '/dashboard', 'as' => 'dashboard.','middleware' => 'user'], function () {

        // Home
        Route::get('/', 'DashBoard\HomeController@index')->name('home');

        // Wallet
        Route::get('/wallet', 'DashBoard\WalletController@index')->name('wallet');
        Route::post('/wallet', 'DashBoard\WalletController@handleWithdraw')->name('wallet');
        Route::get('/withdraw', 'DashBoard\WalletController@getWithdraw')->name('withdraw');
        Route::post('/withdraw', 'DashBoard\WalletController@postWithdraw')->name('withdraw');

        // Wallet
        Route::get('/referrals', 'DashBoard\ReferralController@index')->name('referrals');

        // Binary
        Route::get('/binary/{accountno?}', 'DashBoard\BinaryController@index')->name('binary');

        // Transactions
        Route::get('/transactions', 'DashBoard\TransactionController@index')->name('transactions');

        Route::get('/deposit', 'DashBoard\DepositController@deposit')->name('deposit');
        Route::post('/deposit', 'DashBoard\DepositController@postDeposit')->name('deposit');

        // Commission
        Route::get('/commission', 'DashBoard\CommissionController@index')->name('commission.index');
        Route::post('/commission/email', 'DashBoard\CommissionController@handleEmail')->name('commission.email');
        Route::get('/commission/withdraw', 'DashBoard\CommissionController@getCommission')->name('commission');
        Route::post('/commission/withdraw', 'DashBoard\CommissionController@postCommission')->name('commission');
        Route::get('/commission/binary', 'DashBoard\CommissionController@getBinary')->name('binary.commission');
        Route::post('/commission/binary', 'DashBoard\CommissionController@postBinary')->name('binary.commission');
        Route::get('/monthly/withdraw', 'DashBoard\CommissionController@getMonthly')->name('monthly');
        Route::post('/monthly/withdraw', 'DashBoard\CommissionController@postMonthly')->name('monthly');

        // User
        Route::group(['prefix' => '/user', 'as' => 'user.'], function () {
            Route::get('/overview', 'DashBoard\UserController@overview')->name('overview');

            // Profile
            Route::get('/profile', 'DashBoard\UserController@profile')->name('profile');
            Route::put('/profile', 'DashBoard\UserController@updateProfile')->name('profile.update');
            Route::put('/profile/password', 'DashBoard\UserController@updatePassword')->name('profile.password');
        });

        // Contact
        Route::get('/contact', 'DashBoard\ContactController@index')->name('contact');
        Route::post('/contact', 'DashBoard\ContactController@sendMail')->name('contact.send');

        // Calendar
        Route::get('/calendar', function () {
            return view('dashboard.calendar.index');
        })->name('calendar');

    });

    /**
     * ADMIN PAGE
     */
    Route::group(['prefix' => '/admin', 'as' => 'admin.','middleware' => 'admin'], function () {
        // Home
        Route::get('/', function () {
            return view('admin.home.index');
        })->name('home');

        Route::get('/kickdeposit', 'Admin\DepositController@deposit')->name('kickdeposit');
        Route::post('/kickdeposit', 'Admin\DepositController@postDeposit')->name('kickdeposit');

        Route::get('/countprofit/monthly', 'Admin\ProfitMonthlyController@index')->name('countprofit');

        // Pay
        Route::group(['prefix' => '/pay', 'as' => 'pay.'], function () {

            Route::get('/profit', 'Admin\PaymentController@profit')->name('profit');
            Route::post('/profit/{id}', 'Admin\PaymentController@withdrawProfit')->name('profit.withdraw');

            Route::get('/interest', 'Admin\PaymentController@interest')->name('interest');
            Route::post('/interest/{id}', 'Admin\PaymentController@payInterest')->name('interest.withdraw');

            Route::get('/wallet', 'Admin\PaymentController@wallet')->name('wallet');
            Route::post('/wallet/{id}', 'Admin\PaymentController@withdrawWallet')->name('wallet.withdraw');
        });

        Route::get('/binary', 'Admin\BinaryController@index')->name('binary');
        Route::post('/binary/{id}', 'Admin\BinaryController@payment')->name('binary.payment');

        // ADmin
        Route::group(['prefix' => '/user', 'as' => 'user.'], function () {
            Route::get('/overview', 'Admin\UserController@overview')->name('overview');

            // Profile
            Route::get('/profile', 'Admin\UserController@profile')->name('profile');
            Route::put('/profile', 'Admin\UserController@updateProfile')->name('profile.update');
            Route::put('/profile/password', 'Admin\UserController@updatePassword')->name('profile.password');
        });

        Route::get('/transfer', 'Admin\TransferController@getTransfer')->name('transfer');
        Route::post('/transfer', 'Admin\TransferController@postTransfer')->name('transfer');
    });
});
