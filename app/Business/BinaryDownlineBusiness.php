<?php

namespace App\Business;

use App\Models\BinaryDownline;

class BinaryDownlineBusiness extends Business
{

    /**
     * Instance $binaryDownLine
     *
     * @var BinaryDownline
     */
    private $binaryDownLine;

    /**
     * BinaryDownlineBusiness constructor.
     *
     * @param BinaryDownline $binaryDownline
     */
    public function __construct(BinaryDownline $binaryDownline)
    {
        $this->binaryDownLine = $binaryDownline;
    }

    /**
     * Where BinaryDownline
     *
     * @param $column
     * @param null $operator
     * @param null $value
     * @param string $boolean
     *
     * @return BinaryDownline
     */
    public function where($column, $operator = null, $value = null, $boolean = 'and')
    {
        return $this->binaryDownLine->where($column, $operator, $value, $boolean);
    }

    public function selectBinarybyAccount ($accNo)
    {
        return $this->binaryDownLine->where('accountno', $accNo)->first();
    }

    public function updateDepositAmount ($accno, $deposit_amount)
    {
        return $this->binaryDownLine->where('accountno', $accno)->update(['deposit_amount' => $deposit_amount]);
    }

    public function getBinaryDeposit ($deposit_amount, $amount) {
        $new_binary_deposit = $deposit_amount + $amount;
        return (double)$new_binary_deposit;
    }

    public function depositAndUpdateTransaction ($deposit_amount, $amount, $us_account)
    {
        $new_binary_deposit = self::getBinaryDeposit($deposit_amount, $amount);

        $isUpdate = $this->updateDepositAmount($us_account, $new_binary_deposit);

        if ($isUpdate) return true;

        return false;
    }

}