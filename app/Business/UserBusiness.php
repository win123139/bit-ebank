<?php

namespace App\Business;

use App\Models\BinaryDownline;
use App\User;
use Carbon\Carbon;
use DB;
use LaraBlockIo;

class UserBusiness extends Business
{

    /**
     * Instant $user
     *
     * @var User
     */
    protected $user;

    /**
     * Instance $binaryDownLine
     *
     * @var BinaryDownline
     */
    protected $binaryDownLine;

    /**
     * UserBusiness constructor.
     *
     * @param User $user
     * @param BinaryDownline $binaryDownline
     */
    public function __construct(User $user, BinaryDownline $binaryDownline)
    {
        $this->user = $user;
        $this->binaryDownLine = $binaryDownline;
    }

    /**
     * Find User
     *
     * @param $id
     *
     * @return User
     */
    public function find($id)
    {
        return $this->where('id', $id)->first();
    }

    /**
     * Where User
     *
     * @param $column
     * @param null $operator
     * @param null $value
     * @param string $boolean
     *
     * @return User
     */
    public function where($column, $operator = null, $value = null, $boolean = 'and')
    {
        return $this->user->where($column, $operator, $value, $boolean);
    }

    /**
     * Paginate User
     *
     * @param null $perPage
     * @param array $columns
     * @param string $pageName
     * @param null $page
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginate($perPage = null, $columns = ['*'], $pageName = 'page', $page = null)
    {
        return $this->user->paginate($perPage, $columns, $pageName, $page);
    }

    /**
     * Insert User
     *
     * @param \App\Http\Requests\Auth\RegisterRequest $data
     *
     * @return User|null
     */
    public function insertUser($data, $referralNo)
    {
        try {
            \DB::beginTransaction();

            /** @var $user User */
            $user = new $this->user;
            $user->id = md5(uniqid(rand()));
            $user->gender = $data->input('gender');
            $user->firstname = $data->input('firstname');
            $user->lastname = $data->input('lastname');
            $user->ccno = $data->input('ccno');
            $user->email = $data->input('email');
            $user->country = $data->input('country');
            $user->birthday = $data->input('birthday');
            $user->referral = $referralNo;
            $user->password = md5($data->input('password'));
            $user->accountno = 'BB' . Carbon::now()->timestamp;
            $laraBLockResp = LaraBlockIo::createAddress($user->accountno);
            if ($laraBLockResp->status == 'success') {
                $user->walletaddress = $laraBLockResp->data->address;
            }
            $user->type = 1;
            $user->created_date = Carbon::now()->toDateString();
            $user->save();

            // Update Binary Downline for upline user
            $level = 0;
            $uplineAcc = $this->binaryDownLine->where('accountno', $data->input('upline_acc'))->first();
            if (!is_null($uplineAcc)) {
                $level = $uplineAcc->binarylevel + 1;
                $this->updateBinaryDownLine($uplineAcc, $user, $data->input('leg'));
            }

            // Inser Binary Downline for new user
            $this->insertBinaryDownline($user, $level);

            \DB::commit();

            return $user;

        } catch (\Exception $e) {
            \Log::error('UserBusiness::insertUser : ' . $e->getMessage());
            \DB::rollBack();

            return null;
        }

    }

    /**
     * Insert Binary Down Line
     *
     * @param $user User
     * @param $binaryLevel
     */
    public function insertBinaryDownline($user, $binaryLevel)
    {
        /** @var $binaryDownline BinaryDownline */
        $binaryDownline = new $this->binaryDownLine;
        $binaryDownline->accountno = $user->accountno;
        $binaryDownline->left_acc = '';
        $binaryDownline->right_acc = '';
        $binaryDownline->name = $user->getFullName();
        $binaryDownline->binarylevel = $binaryLevel;
        $binaryDownline->deposit_amount = 0;
        $binaryDownline->save();
    }

    /**
     * Update Binary Down Line For Upline User
     *
     * @param $upline BinaryDownline
     * @param $user   User
     * @param $leg
     */
    public function updateBinaryDownLine($upline, $user, $leg)
    {
        if ($leg == 0) {
            $upline->left_acc = $user->accountno;
        } elseif ($leg == 1) {
            $upline->right_acc = $user->accountno;
        }
        $upline->save();
    }

    /**
     * Get List
     *
     * @param $page
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getList($page, $numReferrals = 2)
    {
        $referals = DB::table('user')
            ->select('referral')
            ->groupBy('referral')
            ->havingRaw('COUNT(*) >= ' . $numReferrals)->get('referral')->toArray();
        $referals = array_map(function ($value) {
            return $value->referral;
        }, $referals);

        return $this->user->whereIn('accountno', $referals)->paginate($page);
    }
}