<?php

namespace App\Business;

use App\Models\TransactionHistory;
use App\Models\UserProfit;
use Auth;
use Illuminate\Support\Facades\DB;
use Mail;
use App\User;
class WalletBusiness extends Business
{

	private $_userProfit;
	private $_transHistory;
    private $_user;

	public function __construct(UserProfit $userProfit, TransactionHistory $transHistory, User $user)
    {
        $this->_userProfit 	    = $userProfit;
        $this->_transHistory    = $transHistory;
        $this->_user            = $user;
    }

    public function handleWallet ()
    {
    	$ustype 	= Auth::user()->type;
    	$accountno 	= Auth::user()->accountno;
    }

    public function updateUserToken ($usid, $token)
    {
        $user = $this->_user->find($usid);

        $user->token = $token;

        $user->save();
    }

    public function getToken ($usid)
    {
        $user = $this->_user->find($usid);
        
        if ($user) {
            return $user->token;
        }

        return null;
    }

    public function selectProfitById ($usid)
    {
        return $this->_userProfit->find($usid);
    }

    public function addProfit ($usid)
    {
        $profit = new UserProfit();

        $profit->user_id            = $usid;
        $profit->last_update        = date("Y-m-d");

        $profit->save();

        return $profit;
    }

    public function setDataForCommission ($profit)
    {
        $data['referral_profit']    = (double)$profit->referral_profit;
        $data['binary_profit']      = (double)$profit->binary_profit;
        $data['deposit_profit']     = (double)$profit->deposit_profit;

        return $data;
    }

    public function selectTransactionWithdrawfromWallet ($usid) 
    {
        return $this->_transHistory
                ->where('usid', $usid)
                ->where('type', 'Withdraw from Wallet')
                ->orderBy('id', 'desc')
                ->paginate(10);
    }

    public function updateWalletAmount ($usid, $wallet_amount)
    {        
        $wallet_amount = (double) $wallet_amount;

        $date = date("Y-m-d");

        $profit = $this->_userProfit->find($usid);

        $profit->btc_wallet_amount = $wallet_amount;

        return $profit->save();
    }

    public function handleTransactionUpdate ($newprofit, $type, $status)
    {
        $transaction_id = strtotime(date('Y-m-d H:i:s'));
        
        return $this->insertTransaction($transaction_id, Auth::user()->id, $type, Auth::user()->walletaddress, $newprofit, $status);
    }

    public function insertTransaction ($transaction_id, $usid, $type, $receive_wallet, $amount, $status) {
        $transHistory                       = new $this->_transHistory;

        $transHistory->id                   = $transaction_id;
        $transHistory->usid                 = $usid;
        $transHistory->date                 = date("Y-m-d");
        $transHistory->type                 = $type;
        $transHistory->receive_wallet       = $receive_wallet;
        $transHistory->amount               = $amount;
        $transHistory->status               = $status;

        return $transHistory->save();
    }

    public function excecuteWithdraw ($usid, $wallet_amount, $transaction_id, $type, $receive_wallet, $amount, $status) {
        $isUpdateAmount         = $this->updateWalletAmount($usid, $wallet_amount);
        $isInsertTransaction    = $this->insertTransaction($transaction_id, $usid, $type, $receive_wallet, $amount, $status);

        if($isUpdateAmount && $isInsertTransaction) return true;
        return false;
    }

    public function handleWithdrawTransaction ($withdraw_amount, $wallet_amount, $transaction_id, $type, $receive_wallet, $btc_withdraw, $status) {
        $withdraw_amount_temp = $withdraw_amount - $wallet_amount;

        $wallet_amount = 0;

        $withdraw_amount_temp = $withdraw_amount_temp - 0.003;

        // Withdraws amount of coins from specific addresses in your account.
        // $withdrawReponse = \LaraBlockIo::withdrawFromAddressesToAddresses($withdraw_amount_temp, $from_wallet, '34xAM6QqX8PaJxboK2EPANqCHX6AhXiyjd');

        $result_check = $this->insertTransaction($transaction_id, Auth::user()->id, $type, $receive_wallet, $btc_withdraw, $status);
        $fullname = Auth::user()->firstname.' '.Auth::user()->lastname;
        $title = 'User '.$fullname.' want to withdraw from wallet';

        Mail::send('dashboard.email.withdraw-mail', 
        array('fullname' => $fullname, 'receive_wallet' => $receive_wallet, 'withdraw_amount' => $withdraw_amount), 
        function ($message) use($title) {
            $message->to(Auth::user()->email, 'Bit-Ebank')->subject($title);
        });

        return $result_check;
    }

    public function checkWithdrawAmount ($btc_withdraw) {
        $response = \LaraBlockIo::getAddressesBalanceByLabels(Auth::user()->accountno);

        if($response->status == 'success') {
            $available_balance  = $response->data->available_balance;
        }

        if ($btc_withdraw > $available_balance) return false;
        return true;
    }

    public function sendEmail ()
    {
        $verify_code = mt_rand(100000, 999999);
        $this->updateUserToken(Auth::user()->id, $verify_code);

        $email = Auth::user()->email;
        $fullname = Auth::user()->firstname.' '.Auth::user()->lastname;

        Mail::send('dashboard.email.mail', 
            array('fullname' => $fullname, 'verify_code' => $verify_code), 
            function ($message) {
                $message->to(Auth::user()->email, 'Bit-Ebank')->subject('Verify code for Withdraw');
        });
    }

    public function updateReferralProfit ($usid, $referral_profit)
    {
        $profit = $this->_userProfit->find($usid);

        $profit->referral_profit = $referral_profit;

        return $profit->save();
    }

    public function updateDepositProfit ($usid, $deposit_profit)
    {
        $profit = $this->_userProfit->find($usid);

        $profit->deposit_profit = $deposit_profit;

        return $profit->save();
    }

    public function updateBinaryProfit ($usid, $binary_profit)
    {
        $profit = $this->_userProfit->find($usid);

        $profit->binary_profit = $binary_profit;

        return $profit->save();
    }
}
