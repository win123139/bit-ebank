<?php

namespace App\Business;

use App\Models\Deposit;
use Auth;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Models\UserProfit;
use Carbon\Carbon;

class DepositBusiness extends Business
{

	private $_deposit;
    private $_user;
    private $_profit;

	public function __construct(Deposit $deposit, User $user, UserProfit $profit)
    {
        $this->_deposit     = $deposit;
        $this->_user        = $user;
        $this->_profit      = $profit;
    }

    public function getUserByEmail ($email)
    {
        return $this->_user->where('email', $email)->first();
    }

    public function getUserByAccNo ($accno)
    {
        return $this->_user->where('accountno', $accno)->first();
    }

    public function insertDeposit ($amount, $type, $usid, $status)
    {
        $deposit_id = uniqid(rand());
        $start_date = date('Y/m/d');

        $deposit = new $this->_deposit;

        $deposit->id            = $deposit_id;
        $deposit->start_date    = $start_date;
        $deposit->type          = $type;
        $deposit->amount        = $amount;
        $deposit->rate          = 20;
        $deposit->user_id       = $usid;
        $deposit->status        = $status;

        return $deposit->save();
    }

    public function updateReferralProfit ($usid, $amount)
    {
        $response = file_get_contents('https://api.coinmarketcap.com/v1/ticker/bitcoin/');
        $response = json_decode($response, true);
        $usd = $response[0]["price_usd"];
        $today_ratio = (double)$usd;
        $referral_profit = (double)((10*$amount)/100) * $today_ratio;

        $profit = $this->_profit->find($usid);

        $profit->referral_profit = $referral_profit;
        
        return $profit->save();
    }

    /**
     * Where Deposit
     *
     * @param $column
     * @param null $operator
     * @param null $value
     * @param string $boolean
     *
     * @return Deposit
     */
    public function where($column, $operator = null, $value = null, $boolean = 'and')
    {
        return $this->_deposit->where($column, $operator, $value, $boolean);
    }

    /**
     * Get List Paginate By Date Column and Status
     *
     * @param $pages
     * @param $dateColumn
     * @param string $status
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getListByStatus($pages, $dateColumn, $status = 'Active')
    {
        return $this->where('status', $status)
            ->where($dateColumn, '<=', Carbon::now()->subMonth()->toDateString())
            ->orWhere(function($query) use ($dateColumn) {
                $query->whereNull($dateColumn)->where('start_date', '<=', Carbon::now()->subMonth()->subDay()->toDateString());
            })
            ->orderBy('start_date', 'ASC')
            ->paginate($pages);
    }

    /**
     * Update Last withdraw column
     *
     * @param $id
     * @param $date
     *
     * @return Deposit
     */
    public function updateLastWithdrawDate($id, $date)
    {
        /** @var $deposit Deposit */
        $deposit = $this->where('id', $id)->first();
        $deposit->last_date_withdraw = $date;
        $deposit->save();

        return $deposit;
    }

    public function checkAccountBalance ($receive_wallet, $amount)
    {
        $balanceRes = \LaraBlockIo::getAddressesBalanceByAddress($receive_wallet);
        if ($balanceRes->data->available_balance <= $amount) return false;
        return true;
    }

    public function getDepositPackages ($userId)
    {
        $packages = $this->_deposit->where('user_id', $userId)->get();

        if (count($packages)) {
            foreach ($packages as $package) {
                $package->pay_at = Carbon::createFromFormat('Y-m-d', $package->start_date)->addMonth()->addDay()->toDateString();
            }
        }

        return (count($packages)) ? $packages : null;
    }
}