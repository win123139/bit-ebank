<?php

namespace App\Business;

use App\Models\TransactionHistory;
use App\User;
use Carbon\Carbon;
use DB;
use Exception;
use Log;

class TransactionBusiness extends Business
{

    /**
     * Instant $transactionHistory
     *
     * @var TransactionHistory
     */
    protected $transactionHistory;

    /**
     * Instant $user
     *
     * @var User
     */
    protected $user;

    public function __construct(TransactionHistory $transactionHistory, User $user)
    {
        $this->transactionHistory = $transactionHistory;
        $this->user = $user;
    }

    /**
     * Where Transaction
     *
     * @param $column
     * @param null $operator
     * @param null $value
     * @param string $boolean
     *
     * @return TransactionHistory
     */
    public function where($column, $operator = null, $value = null, $boolean = 'and')
    {
        return $this->transactionHistory->where($column, $operator, $value, $boolean);
    }

    /**
     * Get List By Type
     *
     * @param $page
     * @param string $type
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getListPendingByType($page, $type = '')
    {
        if($type !=''){
        return $this->where('type', $type)->where('status', 'Pending')->orderBy('created_at','DESC')->paginate($page);
        }
        return $this->where('status', 'Pending')->orderBy('created_at','DESC')->paginate($page);

    }

    /**
     * Pay Money
     *
     * @param $transactionId
     * @param \Illuminate\Http\Request $data
     *
     * @return bool
     */
    public function payMoney($transactionId, $data)
    {
        try {
            $amount = $data->input('amount');
            $receive_address = $data->input('receive_wallet');

            $laraBlockResp = \LaraBlockIo::withdraw($amount, $receive_address);
            if ($laraBlockResp->status == 'success') {
                if ($this->updateStatus($transactionId)) {
                    return true;
                }
            }

            return false;
        } catch (Exception $e) {
            Log::error('TransactionBusiness::payMoney : ' . $e->getMessage());

            return false;
        }
    }

    /**
     * Update Status
     *
     * @param $transactionId
     * @param string $status
     *
     * @return bool
     */
    public function updateStatus($transactionId, $status = 'Completed')
    {
        try {
            DB::beginTransaction();
            $transaction = $this->where('id', $transactionId)->first();
            $transaction->status = $status;
            $transaction->save();
            DB::commit();

            return true;
        } catch (Exception $e) {
            Log::error('TransactionBusiness::updateStatus : ' . $e->getMessage());
            DB::rollBack();

            return false;
        }
    }

    /**
     * Get User By Id
     *
     * @param $id
     *
     * @return User
     */
    public function getUserById($id)
    {
        return $this->user->where('id', $id)->first();
    }

    /**
     * Insert Transaction
     *
     * @param \Illuminate\Http\Request $data
     * @param $type
     * @param string $status
     *
     * @return TransactionHistory
     */
    public function insert($userId, $amount, $type, $status = 'Completed')
    {
        // Get User By User Id
        $user = $this->getUserById($userId);

        /** @var $transaction TransactionHistory */
        $transaction = new $this->transactionHistory;
        $transaction->id = uniqid(rand());
        $transaction->usid = $user->id;
        $transaction->date = Carbon::now()->format('Y-m-d');
        $transaction->type = $type;
        $transaction->receive_wallet = $user->walletaddress;
        $transaction->amount = $amount;
        $transaction->status = $status;
        $transaction->save();

        return $transaction;
    }
}