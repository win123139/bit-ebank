<?php

namespace App\Business;

use App\Models\BinaryTemp;
use App\Models\Deposit;

class BinaryTempBusiness extends Business
{

    /**
     * Instance $binaryTemp
     *
     * @var Deposit
     */
    protected $binaryTemp;

    /**
     * BinaryTempBusiness constructor.
     *
     * @param BinaryTemp $binaryTemp
     */
    public function __construct(BinaryTemp $binaryTemp)
    {
        $this->binaryTemp = $binaryTemp;
    }

    /**
     * Where $binaryTemp
     *
     * @param $column
     * @param null $operator
     * @param null $value
     * @param string $boolean
     *
     * @return $binaryTemp
     */
    public function where($column, $operator = null, $value = null, $boolean = 'and')
    {
        return $this->binaryTemp->where($column, $operator, $value, $boolean);
    }

    /**
     * Update Or Create
     *
     * @param array $attributes
     * @param array $values
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function updateOrCreate($attributes, array $values = [])
    {
        return $this->binaryTemp->updateOrCreate($attributes, $values);
    }
}