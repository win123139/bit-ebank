<?php

namespace App\Business;

use App\Models\UserProfit;
use Carbon\Carbon;

class UserProfitBusiness extends Business
{

    /**
     * Instant $userProfit
     *
     * @var UserProfit
     */
    protected $userProfit;

    /**
     * UserProfitBusiness constructor.
     *
     * @param UserProfit $userProfit
     */
    public function __construct(UserProfit $userProfit)
    {
        $this->userProfit = $userProfit;
    }

    /**
     * Find UserProfit
     *
     * @param $id
     *
     * @return UserProfit
     */
    public function find($id)
    {
        return $this->where('user_id', $id)->first();
    }

    /**
     * Where UserProfit
     *
     * @param $column
     * @param null $operator
     * @param null $value
     * @param string $boolean
     *
     * @return UserProfit
     */
    public function where($column, $operator = null, $value = null, $boolean = 'and')
    {
        return $this->userProfit->where($column, $operator, $value, $boolean);
    }

    /**
     * Update User Profit by user Id and amount
     *
     * @param $userId
     * @param $amount
     *
     * @return UserProfit
     */
    public function update($userId, $amount)
    {
        $userProfit = $this->where('user_id', $userId)->first();
        $userProfit->last_update = Carbon::today()->format('Y-m-d');
        $response = file_get_contents('https://api.coinmarketcap.com/v1/ticker/bitcoin/');
        $response = json_decode($response, true);
        $usd = $response[0]["price_usd"];
        $amount = $amount * $usd;
        $userProfit->binary_profit = $userProfit->binary_profit + $amount;
        $userProfit->save();

        return $userProfit;
    }

    /**
     * Update User Deposit Profit by user Id and amount
     *
     * @param $userId
     * @param $amount
     *
     * @return UserProfit
     */
    public function updateDepositProfit($userId, $amount)
    {
        $userProfit = $this->where('user_id', $userId)->first();
        $userProfit->last_update = Carbon::today()->format('Y-m-d');
        $userProfit->deposit_profit = $userProfit->deposit_profit + $amount;
        $userProfit->save();

        return $userProfit;
    }

}