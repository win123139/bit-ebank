<?php

namespace App;

use App\Business\BinaryDownlineBusiness;
use App\Models\BinaryDownline;
use App\Models\BinaryTemp;
use App\Models\Deposit;
use App\Models\UserProfit;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = "user";

    protected $casts = [
        'id' => 'string',
    ];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'gender', 'firstname', 'lastname', 'ccno', 'email', 'country', 'birthday', 'referral', 'password', 'accountno', 'walletaddress', 'type', 'created_date'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password'
    ];


    /**
     * Get Remember Token
     *
     * @return null
     */
    public function getRememberTokenName()
    {
        return null;
    }

    /**
     * Relation has one Binary Downline
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function binaryDownline()
    {
        return $this->hasOne(BinaryDownline::class, 'accountno', 'accountno');
    }

    /**
     * Relation has many Deposit
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function deposits()
    {
        return $this->hasMany(Deposit::class, 'user_id', 'id');
    }

    /**
     * Reltion has one UserProfit
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function userProfit()
    {
        return $this->hasOne(UserProfit::class, 'user_id', 'id');
    }

    /**
     * Reltion has one BinaryTemp
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function binaryTemp()
    {
        return $this->hasOne(BinaryTemp::class, 'accountno', 'accountno');
    }

    /**
     * Get Full Name
     *
     * @return string
     */
    public function getFullName()
    {
        return $this->firstname . ' ' . $this->lastname;
    }

    /**
     * Get Deposit Amount
     *
     * @return mixed
     */
    public function getDepositAmount()
    {
        return $this->deposits()->get()->sum('amount');
    }

    /**
     * Get Wallet Amount from BlockIO
     *
     * @return string
     */
    public function getWalletBlockAmount()
    {
        try {
            $walletAmount = is_null($this->userProfit) ? 0 : $this->userProfit->btc_wallet_amount;
            $laraBlockResp = \LaraBlockIo::getAddressesBalanceByLabels($this->accountno);
            if ($laraBlockResp->status == 'success') {
                $walletAmount += (double)$laraBlockResp->data->available_balance;
            }

            return number_format($walletAmount, 3);
        } catch (\Exception $e) {
            \Log::error('App\User::getWalletBlockAmount : ' . $e->getMessage());

            return number_format(0, 3);
        }
    }

    /**
     * Get Referrals of user
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function getReferrals()
    {
        return $this->hasMany(self::class, 'referral', 'accountno');
    }

    public function getBinaryLastLeftVenue()
    {
        return is_null($this->binaryTemp) ? 0 : $this->binaryTemp->last_left_venue;
    }

    public function getBinaryLastRightVenue()
    {
        return is_null($this->binaryTemp) ? 0 : $this->binaryTemp->last_right_venue;
    }
}
