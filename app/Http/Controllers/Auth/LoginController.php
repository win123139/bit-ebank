<?php

namespace App\Http\Controllers\Auth;

use App\Business\UserBusiness;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use Auth;
use Exception;
use Log;

class LoginController extends Controller
{
    /**
     * Instance $userBusiness
     *
     * @var UserBusiness
     */
    protected $userBusiness;

    /**
     * LoginController constructor.
     *
     * @param UserBusiness $userBusiness
     */
    public function __construct(UserBusiness $userBusiness)
    {
        $this->userBusiness = $userBusiness;
    }

    /**
     * Show Login Form
     *
     * @return \View
     */
    public function showLoginForm()
    {
        return view('auth.login');
    }

    /**
     * Login Method
     *
     * @param LoginRequest $request
     *
     * @return \Response
     */
    public function login(LoginRequest $request)
    {
        try {
            $user = $this->userBusiness->where('email', $request->input('email'))
                ->where('password', md5($request->input('password')))
                ->where('ccno', $request->input('ccno'))->first();
            if (!is_null($user)) {
                Auth::login($user);

                return redirect()->route('dashboard.home');
            }

            return redirect()->route('auth.login.form')->with([
                'status'  => 'danger',
                'message' => 'The email address. password or ccno that you entered is not valid!!',
                'email'   => $request->input('email'),
                'ccno'    => $request->input('ccno')
            ]);
        } catch (Exception $e) {
            Log::error('LoginController::login : ' . $e->getMessage());

            return redirect()->route('auth.login.form')->with([
                'status'  => 'danger',
                'message' => 'Something went wrong!!'
            ]);;
        }
    }

    /**
     * Logout Method
     *
     * @return \Response
     */
    public function logout()
    {
        try {
            Auth::logout();

            return redirect()->route('auth.login.form');
        } catch (Exception $e) {
            Log::error('LoginController::logout : ' . $e->getMessage());

            return redirect()->route('auth.login.form');
        }

    }
}
