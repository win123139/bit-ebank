<?php

namespace App\Http\Controllers\Auth;

use App\Business\UserBusiness;
use App\Http\Controllers\Controller;
use App\Mail\ForgotPassword;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Mail;

class ForgotPasswordController extends Controller
{
    /**
     * Instance $userBusiness
     *
     * @var UserBusiness
     */
    protected $userBusiness;

    public function __construct(UserBusiness $userBusiness)
    {
        $this->userBusiness = $userBusiness;
    }

    /**
     * @return \View
     */
    public function index()
    {
        return view('auth.forgot');
    }


    /**
     * Send Email Method
     *
     * @param Request $request
     *
     * @return \Response
     */
    public function sendEmail(Request $request)
    {
        try {
            $user = $this->userBusiness->where('email', $request->input('email'))->first();

            // If not found
            if (is_null($user)) {
                return redirect()->route('auth.forgot')->with([
                    'status'  => 'danger',
                    'message' => 'Invalid email. Please try again !'
                ]);
            }

            // Send email
            Mail::to($request->input('email'))->send(new ForgotPassword($user));

            return redirect()->route('auth.forgot')->with([
                'status'  => 'success',
                'message' => 'We sent new password to your email. Please check it'
            ]);
        } catch (\Exception $e) {
            \Log::error('ForgotPasswordController::sendEmail : ' . $e->getMessage());

            return redirect()->route('auth.forgot')->with([
                'status'  => 'danger',
                'message' => 'Something went wrong!!!!'
            ]);
        }
    }

    public function verifyMail($token)
    {
        try {
            $user = $this->userBusiness->where('token', $token)->first();
            if (!is_null($user)) {
                if (Carbon::now()->diffInDays($user->updated_at) <= 2) {
                    \DB::beginTransaction();
                    $newPassword = mt_rand(100000, 999999);
                    $user->token = null;
                    $user->password = md5($newPassword);
                    $user->save();
                    \DB::commit();

                    return view('auth.forgot_notify', compact('newPassword', 'user'));
                }
            }

            return redirect()->route('auth.login.form');
        } catch (\Exception $e) {
            \Log::error('Auth\ForgotPasswordController::verifyMail : ' . $e->getMessage());
            \DB::rollBack();

            return redirect()->route('auth.login.form');
        }
    }
}
