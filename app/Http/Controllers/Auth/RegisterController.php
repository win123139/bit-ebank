<?php

namespace App\Http\Controllers\Auth;

use App\Business\UserBusiness;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegisterRequest;
use App\Mail\RegisterEmail;
use Auth;
use Mail;

class RegisterController extends Controller
{

    protected $userBusiness;

    public function __construct(UserBusiness $userBusiness)
    {
        $this->userBusiness = $userBusiness;
    }

    /**
     * Show Register Form
     *
     * @return \View
     */
    public function showRegistrationForm($accountno = null, $leg = null)
    {
        return view('auth.register.index', compact('accountno', 'leg'));
    }

    /**
     * @param RegisterRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function register(RegisterRequest $request)
    {
        try {
            $referralNo = is_null($request->input('referral')) ? '' : $request->input('referral');
            if (Auth::check()) {
                $request->input('upline_acc');
                $uplineAcc = $this->userBusiness->where('accountno', $request->input('upline_acc'))->first();
                if (is_null($uplineAcc)) {
                    return redirect()->route('auth.register.form')
                        ->with([
                            'status'  => 'danger',
                            'message' => 'Something went wrong with your upline!!!'
                        ]);
                }
                $referralNo = Auth::user()->accountno;
            }
            $user = $this->userBusiness->insertUser($request, $referralNo);

            // If cant Insert User
            if (is_null($user)) {
                return redirect()->back()->with([
                    'status'  => 'danger',
                    'message' => 'Something went wrong!!!'
                ]);
            }

            // Send Mail Request
            try{
                Mail::to($request->input('email'))->send(new RegisterEmail($user, $request->input('password')));
            } catch(\Exception $e) {
                \Log::error('RegisterController::register Mail Error : ' . $e->getMessage());
            }
            
            return redirect()->route('dashboard.binary');
        } catch (\Exception $e) {
            \Log::error('RegisterController::register : ' . $e->getMessage());

            return redirect()->back()->with([
                'status'  => 'danger',
                'message' => 'Something went wrong!!!'
            ]);
        }

    }
}
