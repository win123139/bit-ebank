<?php

namespace App\Http\Controllers\Admin;

use App\Business\DepositBusiness;
use App\Http\Controllers\Controller;

class ProfitMonthlyController extends Controller
{

    protected $depositBusiness;

    public function __construct(DepositBusiness $depositBusiness)
    {
        $this->depositBusiness = $depositBusiness;
    }

    public function index()
    {
        $deposits = $this->depositBusiness->getListByStatus(50,'start_date');

        return view('admin.countprofit.month', compact('deposits'));
    }
}
