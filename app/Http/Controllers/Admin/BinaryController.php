<?php

namespace App\Http\Controllers\Admin;

use App\Business\BinaryTempBusiness;
use App\Business\TransactionBusiness;
use App\Business\UserBusiness;
use App\Business\UserProfitBusiness;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;

class BinaryController extends Controller
{
    protected $userBusiness;

    protected $binaryTempBusiness;

    protected $transactionBusiness;

    protected $userProfitBusiness;

    /**
     * BinaryController constructor.
     *
     * @param UserBusiness $userBusiness
     * @param BinaryTempBusiness $binaryTempBusiness
     * @param TransactionBusiness $transactionBusiness
     * @param UserProfitBusiness $userProfitBusiness
     */
    public function __construct(UserBusiness $userBusiness,
                                BinaryTempBusiness $binaryTempBusiness,
                                TransactionBusiness $transactionBusiness,
                                UserProfitBusiness $userProfitBusiness)
    {
        $this->userBusiness = $userBusiness;
        $this->binaryTempBusiness = $binaryTempBusiness;
        $this->transactionBusiness = $transactionBusiness;
        $this->userProfitBusiness = $userProfitBusiness;
    }

    public function index()
    {
        $users = $this->userBusiness->getList('30');

        return view('admin.binary.profit', compact('users'));
    }

    /**
     * Payment Method
     *
     * @param Request $request
     *
     * @return \Response
     */
    public function payment(Request $request)
    {
        try {
            \DB::beginTransaction();
            $this->userProfitBusiness->update($request->input('user_id'), $request->input('payamount'));

            $this->transactionBusiness->insert($request->input('user_id'), $request->input('payamount'), 'Binary Profit');
            $this->binaryTempBusiness->updateOrCreate([
                'accountno' => $request->input('accountno')
            ], [
                'last_left_venue'   => $request->input('leftdeposit'),
                'last_right_venue'  => $request->input('rightdeposit'),
                'last_date_payment' => Carbon::today()->timestamp
            ]);
            \DB::commit();

            return redirect()->route('admin.binary')->with([
                'status'  => 'success',
                'message' => 'Your profit paid!!!'
            ]);
        } catch (\Exception $e) {
            \Log::error('Admin\BinaryController::payment : ' . $e->getMessage());
            \DB::rollBack();

            return redirect()->route('admin.binary')->with([
                'status'  => 'danger',
                'message' => 'Something went wrong!!!!'
            ]);
        }
    }
}
