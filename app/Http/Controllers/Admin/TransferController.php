<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TransferController extends Controller
{
    public function getTransfer ()
    {
    	//Check account balance
		$balanceRes 		= \LaraBlockIo::getAddressesBalanceByAddress(env('BLOCKIO_ADDRESS'));
		$data['balance'] 	= $balanceRes->data->available_balance;

    	return view('admin.transfer.index', $data);
    }

    public function postTransfer (Request $request)
    {
    	try {
    		$balanceRes 		= \LaraBlockIo::getAddressesBalanceByAddress(env('BLOCKIO_ADDRESS'));    		
			$balance 			= $balanceRes->data->available_balance;
	    	$amount 			= $request->amount;

            $feeRes = \LaraBlockIo::getNetworkFeeEstimate($amount, env('PRIVATE_WALLET'));

            $fee = $feeRes->data->estimated_network_fee;

	    	if ($balance - $amount < $fee) {
	    		return redirect()->route('admin.transfer')
					->with('error', 'Your balance is not enough to withdraw.');
	    	}

	    	$withdrawReponse = \LaraBlockIo::withdrawFromAddressesToAddresses($amount, env('BLOCKIO_ADDRESS'), env('PRIVATE_WALLET'));	    	

	    	if($withdrawReponse->status == 'success') {
	    		return redirect()->route('admin.transfer')->with('message', 'Withdraw to your wallet successfully.');
	    	}

    	} catch (\Exception $e) {
    		\Log::error('TransferController::postTransfer : ' . $e->getMessage());
            return redirect()->route('admin.transfer')->with('error', 'Something went wrong!!!!');	
    	}
    }
}
