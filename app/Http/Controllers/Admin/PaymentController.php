<?php

namespace App\Http\Controllers\Admin;

use App\Business\DepositBusiness;
use App\Business\TransactionBusiness;
use App\Business\UserProfitBusiness;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    protected $depositBusiness;
    protected $transactionBusiness;
    protected $userProfitBusiness;

    /**
     * PaymentController constructor.
     *
     * @param DepositBusiness $depositBusiness
     * @param TransactionBusiness $transactionBusiness
     * @param UserProfitBusiness $userProfitBusiness
     */
    public function __construct(DepositBusiness $depositBusiness,
                                TransactionBusiness $transactionBusiness, 
                                UserProfitBusiness $userProfitBusiness)
    {
        $this->depositBusiness = $depositBusiness;
        $this->transactionBusiness = $transactionBusiness;
        $this->userProfitBusiness = $userProfitBusiness;
    }

    /**
     * View Profit
     *
     * @return \View
     */
    public function profit()
    {
        $deposits = $this->depositBusiness->getListByStatus(50, 'last_date_withdraw');

        return view('admin.pay.profit', compact('deposits'));
    }

    /**
     * Withdraw Profit Method
     *
     * @param $id
     * @param Request $request
     *
     * @return \Response
     */
    public function withdrawProfit($id, Request $request)
    {
        try {
            \DB::beginTransaction();
            $this->depositBusiness->updateLastWithdrawDate($id, $request->input('date_pay'));
            $this->transactionBusiness->insert($request->input('user_id'),$request->input('amount'),'Monthly Profit');
            $this->userProfitBusiness->updateDepositProfit($request->input('user_id'), $request->input('amount'));
            \DB::commit();

            return redirect()->route('admin.pay.profit')->with([
                'status'  => 'success',
                'message' => 'Your profit paid!!!'
            ]);
        } catch (\Exception $e) {
            \Log::error('PaymentController::withdrawProfit : ' . $e->getMessage());
            \DB::rollBack();

            return redirect()->route('admin.pay.profit')->with([
                'status'  => 'danger',
                'message' => 'Something went wrong!!!!'
            ]);
        }
    }

    /**
     * Wallet View
     *
     * @return \View
     */
    public function wallet()
    {
        $transactions = $this->transactionBusiness->getListPendingByType(50);

        return view('admin.pay.wallet', compact('transactions'));
    }

    /**
     * Withdraw Money From Wallet Transaction Post Method
     *
     * @param $id
     * @param Request $request
     *
     * @return \Response
     */
    public function withdrawWallet($id, Request $request)
    {
        try {
            if ($this->transactionBusiness->payMoney($id, $request)) {
                return redirect()->route('admin.pay.wallet')->with([
                    'status'  => 'success',
                    'message' => 'Your withdraw was paid!!!'
                ]);
            }

            return redirect()->route('admin.pay.wallet')->with([
                'status'  => 'danger',
                'message' => 'Something went wrong!!!!'
            ]);
        } catch (\Exception $e) {
            \Log::error('PayWalletController::withdrawWallet : ' . $e->getMessage());

            return redirect()->route('admin.pay.wallet')->with([
                'status'  => 'danger',
                'message' => 'Something went wrong!!!!'
            ]);
        }
    }

    public function interest()
    {
        $deposits = $this->depositBusiness->getListByStatus(15, 'last_date_withdraw');

        return view('admin.pay.interest', compact('deposits'));
    }

    public function payInterest($id, Request $request)
    {
        try {
            \DB::beginTransaction();
            $this->depositBusiness->updateLastWithdrawDate($id, $request->input('date_pay'));

            $this->transactionBusiness->insert($request->input('user_id'),($request->input('amount')*5)/100,'Monthly Profit');
            $this->transactionBusiness->insert($request->input('user_id'),($request->input('amount')*15)/100,'Monthly Profit');
            \DB::commit();

            return redirect()->route('admin.pay.interest')->with([
                'status'  => 'success',
                'message' => 'User profit was paid!!!'
            ]);
        } catch (\Exception $e) {
            \Log::error('PaymentController::payInterest : ' . $e->getMessage());
            \DB::rollBack();

            return redirect()->route('admin.pay.interest')->with([
                'status'  => 'danger',
                'message' => 'Something went wrong!!!!'
            ]);
        }
    }
}
