<?php

namespace App\Http\Controllers\Admin;

use App\Business\BinaryDownlineBusiness;
use App\Business\DepositBusiness;
use App\Business\WalletBusiness;
use App\Http\Controllers\Controller;
use DB;
use Exception;
use Illuminate\Http\Request;
use Auth;

class DepositController extends Controller
{

    private $_deposit;
    private $_wallet;
    private $_binary;

    public function __construct(DepositBusiness $deposit, WalletBusiness $wallet, BinaryDownlineBusiness $binary)
    {
        $this->_deposit = $deposit;
        $this->_wallet = $wallet;
        $this->_binary = $binary;
    }

    public function deposit()
    {
        return view('admin.kickdeposit.index');
    }

    public function postDeposit(Request $request)
    {
        try {

            //Get all request variables
            $plan       = $request->optionsRadios21;
            $amount     = $request->amount;

            $user = $this->_deposit->getUserByEmail($request->email);

            if (!$user) return redirect()->route('admin.kickdeposit')->with('error', 'Invalid email.');

            switch ($plan) {
                case 0:
                    if ($amount < 0.2 || $amount >= 1) return redirect()->route('admin.kickdeposit')->with('error', 'Your deposit amount must be bigger than 0.2 BTC and smaller than 1 BTC.');
                    break;
                case 1:
                    if ($amount < 1 || $amount >= 3) return redirect()->route('admin.kickdeposit')->with('error', 'Your deposit amount must be bigger than 1 BTC and smaller than 3 BTC.');
                    break;
                case 3:
                    if ($amount < 3 || $amount >= 5) return redirect()->route('admin.kickdeposit')->with('error', 'Your deposit amount must be bigger than 3 BTC and smaller than 5 BTC.');
                    break;
                case 5:
                    if ($amount < 5 || $amount >= 7) return redirect()->route('admin.kickdeposit')->with('error', 'Your deposit amount must be bigger than 5 BTC and smaller than 7 BTC.');
                    break;
                case 7:
                    if ($amount < 7 || $amount >= 10) return redirect()->route('admin.kickdeposit')->with('error', 'Your deposit amount must be bigger than 7 BTC and smaller than 10 BTC.');
                    break;
                default:
                    if ($amount <= 10) return redirect()->route('admin.kickdeposit')->with('error', 'Your deposit amount must be bigger than 10 BTC.');
                    break;
            }

            $us_id          = $user->id;
            $us_account     = $user->accountno;
            $us_sponsor     = $user->referral;
            $receive_wallet = $user->walletaddress;

            $userAccNo  = $this->_deposit->getUserByAccNo($us_sponsor);

            $transaction_id = strtotime(date('Y-m-d H:i:s'));

            DB::beginTransaction();
            try {

                //Insert deposit package of user
                $this->_deposit->insertDeposit($amount, $plan, $us_id, 'Active');

                if ($userAccNo) {
                    //Update referral profit into user profit table
                    $this->_deposit->updateReferralProfit($userAccNo->id, $amount);
                }

                //Insert into transaction history
                $this->_wallet->insertTransaction($transaction_id, $us_id, 'Deposit', $receive_wallet, $amount, 'Completed');

                $binaryAcc = $this->_binary->selectBinarybyAccount($us_account);
                if (!$binaryAcc) {
                    return redirect()->route('admin.kickdeposit')->with('error', 'Something went wrong!!!.');
                }

                //update profit in binary downline table
                $isDeposit = $this->_binary->depositAndUpdateTransaction($binaryAcc->deposit_amount, $amount, $us_account);
                if (!$isDeposit) {
                    return redirect()->route('admin.kickdeposit')->with('error', 'Something went wrong!!!.');
                }

                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();

                return redirect()->route('admin.kickdeposit')->with('error', 'Something went wrong!!!.');
            }

            return redirect()->route('admin.kickdeposit')->with('message', 'Kick successfully.');
        } catch (Exception $e) {
            \Log::error('Admin\DepositController::postDeposit: ' . $e->getMessage());

            return redirect()->route('admin.kickdeposit')->with('error', 'Something went wrong!!!.');
        }
    }
}
