<?php

namespace App\Http\Controllers\DashBoard;

use App\Business\UserBusiness;
use App\Http\Controllers\Controller;
use Auth;

class HomeController extends Controller
{
    /**
     * Instance $userBusiness
     *
     * @var UserBusiness
     */
    protected $userBusiness;

    public function __construct(UserBusiness $userBusiness)
    {
        $this->userBusiness = $userBusiness;
    }

    public function index()
    {
        $walletAmount = Auth::user()->getWalletBlockAmount();

        $userProfit = is_null(Auth::user()->userProfit) ? 0 : Auth::user()->userProfit->deposit_profit;
        // Get List Referal Users of Login User via Pagination
        $referrals = $this->userBusiness->where('referral', Auth::user()->accountno)->get();

        $response = json_decode(file_get_contents('https://api.coinmarketcap.com/v1/ticker/bitcoin/'), true);
        $usd = $response[0]["price_usd"];
        $priceUsd = (double)$usd;

        return view('dashboard.home.index', compact('walletAmount', 'userProfit', 'referrals', 'priceUsd'));
    }
}
