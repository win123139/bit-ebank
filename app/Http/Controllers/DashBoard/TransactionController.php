<?php

namespace App\Http\Controllers\DashBoard;

use App\Business\TransactionBusiness;
use App\Http\Controllers\Controller;

class TransactionController extends Controller
{
    protected $transactionBusiness;

    public function __construct(TransactionBusiness $transactionBusiness)
    {
        $this->transactionBusiness = $transactionBusiness;
    }

    public function index()
    {
        $transactions = $this->transactionBusiness
            ->where('usid', \Auth::user()->id)
            ->orderBy('id', 'desc')
            ->paginate(30);

        return view('dashboard.transactions.index', compact('transactions'));
    }
}
