<?php

namespace App\Http\Controllers\DashBoard;

use App\Http\Controllers\Controller;
use App\Http\Requests\DashBoard\ContactRequest;
use App\Mail\ContactEmail;

class ContactController extends Controller
{
    public function index()
    {
        return view('dashboard.contact.index');
    }

    /**
     * Send Mail Method
     *
     * @param ContactRequest $request
     *
     * @return \Response
     */
    public function sendMail(ContactRequest $request)
    {
        try {
            $contactEmail = collect([
                'name'    => $request->input('name'),
                'email'   => $request->input('email'),
                'ccno'    => $request->input('ccno'),
                'message' => $request->input('message')
            ]);
            \Mail::to(env('MAIL_USERNAME'))->send(new ContactEmail($contactEmail));

            return redirect()->route('dashboard.contact')->with([
                'status'  => 'success',
                'message' => 'Your message was sent!!!'
            ]);
        } catch (\Exception $e) {
            \Log::error('DashBoard\ContactController::sendMail : ' . $e->getMessage());

            return redirect()->route('dashboard.contact')->with([
                'status'  => 'danger',
                'message' => 'Something went wrong!!!!'
            ]);
        }
    }
}
