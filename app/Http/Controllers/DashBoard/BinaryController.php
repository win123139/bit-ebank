<?php

namespace App\Http\Controllers\DashBoard;

use App\Business\BinaryDownlineBusiness;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class BinaryController extends Controller
{
    /**
     * Instance $binaryDownlineBusiness
     *
     * @var BinaryDownlineBusiness
     */
    protected $binaryDownlineBusiness;

    /**
     * BinaryController constructor.
     *
     * @param BinaryDownlineBusiness $binaryDownlineBusiness
     */
    public function __construct(BinaryDownlineBusiness $binaryDownlineBusiness)
    {
        $this->binaryDownlineBusiness = $binaryDownlineBusiness;
    }

    public function index($accountno = null)
    {

        // Get Current User Id by Request
        if (is_null($accountno)) {
            $accountno = Auth::user()->accountno;
        }

        // Get Current Users List;
        $curUsers = $this->binaryDownlineBusiness->where('accountno', $accountno)->get();

        // Level of Tree
        $level = 4;

        return view('dashboard.binary.index', compact('curUsers', 'level'));

    }
}
