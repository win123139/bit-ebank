<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Business\WalletBusiness;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Session;
use Auth;
use Mail;

class WalletController extends Controller
{

	private $_walletBusiness;

	public function __construct(WalletBusiness $walletBusiness)
    {
        $this->_walletBusiness = $walletBusiness;
    }

    public function index ()
    {
        $usid = Auth::user()->id;
    	$data['walletAddress'] = Auth::user()->walletaddress;
    	$label = Auth::user()->accountno;

    	//handle display balance
    	$ustype = Auth::user()->type;
    	if($ustype == 5) $data['balance'] = '0.5 BTC';
    	else if($ustype == 2) $data['balance'] = '0.05 BTC';
		else {
			$wallet_amount = 0;
			$profit = $this->_walletBusiness->selectProfitById($usid);

			if($profit) {
				$wallet_amount = $profit->btc_wallet_amount;
			}

			$response = \LaraBlockIo::getAddressesBalanceByLabels($label);
			if($response->status == 'success') {
				$response_array = $response->data->available_balance;
                $wallet_amount = $wallet_amount + (double)$response_array;
            }

            $data['wallet_amount'] = $wallet_amount;
		}

        //Get transaction history info
        $transactionHistory = $this->_walletBusiness->selectTransactionWithdrawfromWallet($usid);
        if($transactionHistory) $data['transactionHistory'] = $transactionHistory;

    	return view('dashboard.wallet.index', $data);
    }

    public function handleWithdraw (Request $request)
    {
        try {
            $verify_code = mt_rand(100000, 999999);
            $this->_walletBusiness->updateUserToken(Auth::user()->id, $verify_code);

            $email = Auth::user()->email;
            $fullname = Auth::user()->firstname.' '.Auth::user()->lastname;

            Mail::send('dashboard.email.mail', 
                array('fullname' => $fullname, 'verify_code' => $verify_code), 
                function ($message) {
                    $message->to(Auth::user()->email, 'Bit-Ebank')->subject('Verify code for Withdraw');
                });

            return redirect()->route('dashboard.withdraw');
        } catch (\Exception $e) {
            \Log::error('Dashboard\WalletController::handleWithdraw : ' . $e->getMessage());

            return redirect()->route('dashboard.withdraw')
                    ->with('error', 'Something went wrong!!!!');
        }
    }

    public function getWithdraw ()
    {
        $usid = Auth::user()->id;
        $data['walletAddress'] = Auth::user()->walletaddress;
        $label = Auth::user()->accountno;

        //handle display balance
        $ustype = Auth::user()->type;
        if($ustype == 5) $data['balance'] = '0.5 BTC';
        elseif($ustype == 2) $data['balance'] = '0.05 BTC';
        else {
            $wallet_amount = 0;
            $profit = $this->_walletBusiness->selectProfitById($usid);

            if($profit) {
                $wallet_amount = $profit->btc_wallet_amount;
            }

            $response = \LaraBlockIo::getAddressesBalanceByLabels($label);
            if($response->status == 'success') {
                $response_array     = $response->data->available_balance;
                $wallet_amount      = $wallet_amount + (double)$response_array;
            }

            $data['wallet_amount'] = $wallet_amount;
        }

        //Get transaction history info
        $transactionHistory = $this->_walletBusiness->selectTransactionWithdrawfromWallet($usid);
        if($transactionHistory) $data['transactionHistory'] = $transactionHistory;

        return view('dashboard.wallet.withdraw', $data);
    }

    public function postWithdraw (Request $request)
    {
        $ccno_post      = $request['ccno'];
        $btc_withdraw   = (double) $request["moneyamount"];
        $receive_wallet = $request["receivewallet"];
        $wallet_amount  = 0;
        $post_verify_code = $request['verify_code'];
        $profit         = $this->_walletBusiness->selectProfitById(Auth::user()->id);

        $verify_code = $this->_walletBusiness->getToken(Auth::user()->id);

        if ($profit) {
            $wallet_amount = $profit->btc_wallet_amount;
        }

        if (!is_null($verify_code) && $verify_code != $post_verify_code) {
            return redirect()->route('dashboard.withdraw')
                    ->with('error', 'Your verify code is invalid.');
        }

        if (Auth::user()->ccno == $ccno_post) {
            if ($btc_withdraw < 0.01) {
                return redirect()->route('dashboard.withdraw')
                    ->with('error', 'Withdraw amount must be larger than 0.01 BTC.');
            } 
            else {
                $canWithdraw = $this->_walletBusiness->checkWithdrawAmount($btc_withdraw);

                if(!$canWithdraw) {
                    return redirect()->route('dashboard.withdraw')
                        ->with('error', 'Your remaining balance is not enough.');
                }
                else {
                    $transaction_id = strtotime(date('Y-m-d H:i:s'));
                    $type = 'Withdraw from Wallet';
                    $status = 'Pending';
                    $from_wallet = Auth::user()->walletaddress;
                    $withdraw_amount = $btc_withdraw - 0.002;

                    if ($withdraw_amount <= $wallet_amount) {
                        $wallet_amount = $wallet_amount - $withdraw_amount;
                        DB::beginTransaction();

                        try {
                            $iswithDrawn = $this->_walletBusiness->excecuteWithdraw(Auth::user()->id, $wallet_amount, $transaction_id, $type, $receive_wallet, $withdraw_amount, $status);

                            DB::commit();
                        } catch (\Exception $e) {
                            DB::rollback();
                            return redirect()->route('dashboard.withdraw')
                                ->with('error', 'Something went wrong!!!!');
                        }
                    }
                    else {
                        $result_check = $this->_walletBusiness->handleWithdrawTransaction($withdraw_amount, $wallet_amount, $transaction_id, $type, $receive_wallet, $btc_withdraw, $status);

                        if ($result_check == 1) {
                            return redirect()->route('dashboard.withdraw')
                                ->with('message', 'Your profit was executed, please wait for 24h until it is sent to your wallet.');
                        } 
                        else {
                            return redirect()->route('dashboard.withdraw')
                                ->with('error', 'Server is under maintenance. Please try after 10 minutes.');
                        }
                    }
                }
            }
        } 
        else {
            return redirect()->route('dashboard.withdraw')
                ->with('error', 'Wrong CCno. Please enter again !');
        }
    }
}