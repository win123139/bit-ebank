<?php

namespace App\Http\Controllers\DashBoard;

use App\Business\UserBusiness;
use App\Http\Controllers\Controller;
use Auth;

class ReferralController extends Controller
{

    /**
     * Instance $userBusiness
     *
     * @var UserBusiness
     */
    protected $userBusiness;

    public function __construct(UserBusiness $userBusiness)
    {
        $this->userBusiness = $userBusiness;
    }

    public function index()
    {
        // Get List Referal Users of Login User via Pagination
        $users = $this->userBusiness->where('referral', Auth::user()->accountno)->paginate('30');

        return view('dashboard.referrals.index', compact('users'));
    }
}
