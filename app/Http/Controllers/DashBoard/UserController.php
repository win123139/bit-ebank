<?php

namespace App\Http\Controllers\DashBoard;

use App\Http\Requests\DashBoard\UserRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Exception;
use Log;
use Validator;

class UserController extends Controller
{
    /**
     * Show Overview
     *
     * @return \View
     */
    public function overview()
    {
        return view('dashboard.user.overview');
    }

    /**
     * Show Profile
     *
     * @return \View
     */
    public function profile()
    {
        return view('dashboard.user.profile');
    }

    /**
     * Update Profile Method
     *
     * @param UserRequest $request
     *
     * @return \Response
     */
    public function updateProfile(UserRequest $request)
    {
        try {
            // Save User
            $user = Auth::user();
            $user->firstname = $request->firstname;
            $user->lastname = $request->lastname;
            $user->birthday = $request->birthday;
            $user->save();

            return redirect()
                    ->route('dashboard.user.profile')
                    ->with([
                        'profile' => 'success',
                        'message' => 'Your profile was changed!!'
                    ]);
        } catch (Exception $e) {
            Log::error('DashBoard\UserController::updateProfile : ' . $e->getMessage());

            return redirect()->route('dashboard.user.profile');
        }
    }

    /**
     * Update Password Method
     *
     * @param Request $request
     *
     * @return \Response
     */
    public function updatePassword(Request $request)
    {
        try {
            // Create Validate
            $validator = Validator::make($request->all(), [
                'current_pass'          => 'required',
                'password'              => 'required|confirmed',
                'password_confirmation' => 'required',
            ],[
                'current_pass.required'                 => 'Current Pass is required!!',
                'password.required'                     => 'Password is required!!',
                'password.confirmed'                    => 'Password is not same with passwor confirm!!',
                'password_confirmation.required'        => 'Password Confirm is required!!',
            ]);
            
            // If Fail Request
            $user = Auth::user();
            if ($validator->fails() || $user->password !== md5($request->input('current_pass'))) {
                return redirect()->route('dashboard.user.profile')
                    ->withErrors($validator)
                    ->with([
                        'password' => 'danger',
                        'message' => 'Your Current Password Wrong!!'
                    ]);
            }

            // Update New password
            $user->password = md5($request->input('password'));
            $user->save();

            return redirect()->route('dashboard.user.profile')->with([
                'password' => 'success',
                'message' => 'Change Compelted!!'
            ]);

        } catch (Exception $e) {
            Log::error('DashBoard\UserController::updatePassword : ' . $e->getMessage());

            return redirect()->route('dashboard.user.profile');
        }
    }

}
