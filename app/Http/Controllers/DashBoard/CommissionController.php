<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Business\WalletBusiness;
use Illuminate\Support\Facades\DB;
use Session;
use Auth;
use Mail;

class CommissionController extends Controller
{
    private $_walletBusiness;

	public function __construct(WalletBusiness $walletBusiness)
    {
        $this->_walletBusiness = $walletBusiness;
    }

    public function index ()
    {
        try {
        	$profit = $this->_walletBusiness->selectProfitById(Auth::user()->id);

            if(!$profit) {
                $newProfit = $this->_walletBusiness->addProfit(Auth::user()->id);
                $data   = $this->_walletBusiness->setDataForCommission($newProfit);
            } else {
                $data   = $this->_walletBusiness->setDataForCommission($profit);
            }

	    	return view('dashboard.commission.index', $data);
        } catch (\Exception $e) {
            \Log::error('Dashboard\CommissionController::index : ' . $e->getMessage());

            return redirect()->route('dashboard.commission.index')
                    ->with('error', 'Something went wrong!!!!');
        }
    }

    public function handleEmail (Request $request)
    {
    	try {

    		//Send verify code to user email
    		$this->_walletBusiness->sendEmail();
            
            if(!is_null($request->input('btn_withdraw_referral'))) return redirect()->route('dashboard.commission');
            if(!is_null($request->input('btn_withdraw_binary'))) return redirect()->route('dashboard.binary.commission');
            if(!is_null($request->input('btn_withdraw_profit'))) return redirect()->route('dashboard.monthly');
    	} catch (\Exception $e) {
            \Log::error('Dashboard\CommissionController::handleEmail : ' . $e->getMessage());

            return redirect()->route('dashboard.commission.index')
                    ->with('error', 'Something went wrong!!!!');
        }
    }

    public function getCommission ()
    {
    	try {
        	$profit = $this->_walletBusiness->selectProfitById(Auth::user()->id);
	        $data 	= $this->_walletBusiness->setDataForCommission($profit);

	        $response = file_get_contents('https://api.coinmarketcap.com/v1/ticker/bitcoin/');
			$response = json_decode($response, true);
			$usd = $response[0]["price_usd"];
			$data['today_ratio'] = (double)$usd;

	    	return view('dashboard.commission.withdraw', $data);
        } catch (\Exception $e) {
            \Log::error('Dashboard\CommissionController::getCommission : ' . $e->getMessage());

            return redirect()->route('dashboard.commission.index')
                    ->with('error', 'Something went wrong!!!!');
        }
    }

    public function getBinary ()
    {
        try {
            $profit = $this->_walletBusiness->selectProfitById(Auth::user()->id);
            $data   = $this->_walletBusiness->setDataForCommission($profit);

            $response = file_get_contents('https://api.coinmarketcap.com/v1/ticker/bitcoin/');
            $response = json_decode($response, true);
            $usd = $response[0]["price_usd"];
            $data['today_ratio'] = (double)$usd;

            return view('dashboard.commission.binary', $data);
        } catch (\Exception $e) {
            \Log::error('Dashboard\CommissionController::getBinary : ' . $e->getMessage());

            return redirect()->route('dashboard.commission.binary')
                    ->with('error', 'Something went wrong!!!!');
        }
    }

    public function postBinary (Request $request)
    {
        try {
            $ccno_post      = $request['ccno'];         
            $newprofit      = (double)$request["moneyamount"];
            $sendprofit     = (double)$request["bitexchanged"];
            $receive_wallet = $request["receivewallet"];
            $post_verify_code = $request['verify_code'];
            $verify_code    = $this->_walletBusiness->getToken(Auth::user()->id);
            $profit         = $this->_walletBusiness->selectProfitById(Auth::user()->id);
            $old_profit     = $profit->binary_profit;

            if (!is_null($verify_code) && $verify_code != $post_verify_code) {
                return redirect()->route('dashboard.binary.commission')
                        ->with('error', 'Your verify code is invalid.');
            }

            $canWithdraw = $this->_walletBusiness->checkWithdrawAmount($newprofit);

            if (Auth::user()->ccno == $ccno_post) {
                if ($newprofit < 0.001) {
                    return redirect()->route('dashboard.binary.commission')
                        ->with('error', 'Withdraw amount must be larger than 0.001 BTC.');
                }

                if ($newprofit > $old_profit) {
                    return redirect()->route('dashboard.binary.commission')
                        ->with('error', 'Your remaining balance is not enough.');
                }

                $binary_profit      = $old_profit - $newprofit;
                $amount_sent        = $sendprofit - 0.005;
                $wallet_amount      = $profit->btc_wallet_amount + $amount_sent;

                DB::beginTransaction();

                try {
                    $isUpdateProfit         = $this->_walletBusiness->updateReferralProfit(Auth::user()->id, $binary_profit);
                    // $isUpdateWallet         = $this->_walletBusiness->updateWalletAmount(Auth::user()->id, $wallet_amount);
                    $isInsertTransaction    = $this->_walletBusiness->handleTransactionUpdate($amount_sent, 'Binary Bonus Withdraw', 'Pending');

                    DB::commit();
                } catch (\Exception $e) {
                    DB::rollback();
                    return redirect()->route('dashboard.binary.commission')
                        ->with('error', 'Something went wrong!!!!');
                }

                return redirect()->route('dashboard.binary.commission')->with('message', 'Your profit was executed, please wait for 24h until it is sent to your wallet.');
            }
        } catch (\Exception $e) {
            \Log::error('Dashboard\CommissionController::postBinary: ' . $e->getMessage());

            return redirect()->route('dashboard.binary.commission')
                    ->with('error', 'Something went wrong!!!!');
        }
    }

    public function getMonthly ()
    {
        try {
            $profit = $this->_walletBusiness->selectProfitById(Auth::user()->id);
            $data   = $this->_walletBusiness->setDataForCommission($profit);

            $response = file_get_contents('https://api.coinmarketcap.com/v1/ticker/bitcoin/');
            $response = json_decode($response, true);
            $usd = $response[0]["price_usd"];
            $data['today_ratio'] = (double)$usd;

            return view('dashboard.commission.monthly', $data);
        } catch (\Exception $e) {
            \Log::error('Dashboard\CommissionController::getMonthly : ' . $e->getMessage());

            return redirect()->route('dashboard.commission.index')
                    ->with('error', 'Something went wrong!!!!');
        }
    }

    public function postMonthly (Request $request)
    {
        try {
            $ccno_post      = $request['ccno'];         
            $newprofit      = (double)$request["moneyamount"];
            $receive_wallet = $request["receivewallet"];
            $post_verify_code = $request['verify_code'];
            $verify_code    = $this->_walletBusiness->getToken(Auth::user()->id);
            $profit         = $this->_walletBusiness->selectProfitById(Auth::user()->id);
            $old_profit     = $profit->deposit_profit;

            if (!is_null($verify_code) && $verify_code != $post_verify_code) {
                return redirect()->route('dashboard.monthly')
                        ->with('error', 'Your verify code is invalid.');
            }

            $canWithdraw = $this->_walletBusiness->checkWithdrawAmount($newprofit);

            if (Auth::user()->ccno == $ccno_post) {
                if ($newprofit < 0.001) {
                    return redirect()->route('dashboard.monthly')
                        ->with('error', 'Withdraw amount must be larger than 0.001 BTC.');
                }

                if ($newprofit > $old_profit) {
                    return redirect()->route('dashboard.monthly')
                        ->with('error', 'Your remaining balance is not enough.');
                }

                $deposit_profit     = $old_profit - $newprofit;
                $amount_sent        = $newprofit - 0.005;
                $wallet_amount      = $profit->btc_wallet_amount + $amount_sent;

                DB::beginTransaction();

                try {
                    $isUpdateProfit         = $this->_walletBusiness->updateDepositProfit(Auth::user()->id, $deposit_profit);
                    // $isUpdateWallet         = $this->_walletBusiness->updateWalletAmount(Auth::user()->id, $wallet_amount);
                    $isInsertTransaction    = $this->_walletBusiness->handleTransactionUpdate($amount_sent, 'Monthly Profit Withdraw', 'Pending');

                    DB::commit();
                } catch (\Exception $e) {
                    DB::rollback();
                    return redirect()->route('dashboard.monthly')
                        ->with('error', 'Something went wrong!!!!');
                }

                return redirect()->route('dashboard.monthly')->with('message', 'Your profit was executed, please wait for 24h until it is sent to your wallet.');
            }
        } catch (\Exception $e) {
            \Log::error('Dashboard\CommissionController::postMonthly: ' . $e->getMessage());

            return redirect()->route('dashboard.monthly')
                    ->with('error', 'Something went wrong!!!!');
        }
    }

    public function postCommission (Request $request)
    {
    	try {
        	$ccno_post      = $request['ccno'];	        
	        $newprofit 		= (double)$request["moneyamount"];
			$sendprofit 	= (double)$request["bitexchanged"];
	        $receive_wallet = $request["receivewallet"];
	        $post_verify_code = $request['verify_code'];
	        $verify_code 	= $this->_walletBusiness->getToken(Auth::user()->id);
	        $profit         = $this->_walletBusiness->selectProfitById(Auth::user()->id);
            $old_profit     = $profit->referral_profit;

	        if (!is_null($verify_code) && $verify_code != $post_verify_code) {
	            return redirect()->route('dashboard.commission')
	                    ->with('error', 'Your verify code is invalid.');
	        }

	        $canWithdraw = $this->_walletBusiness->checkWithdrawAmount($newprofit);

	        if (Auth::user()->ccno == $ccno_post) {
	        	if ($newprofit < 0.001) {
                	return redirect()->route('dashboard.commission')
                    	->with('error', 'Withdraw amount must be larger than 0.001 BTC.');
                }

                if ($newprofit > $old_profit) {
	                return redirect()->route('dashboard.commission')
	                    ->with('error', 'Your remaining balance is not enough.');
	            }

	            $referral_profit 	= $old_profit - $newprofit;
                $amount_sent 		= $sendprofit - 0.005;
                $wallet_amount 		= $profit->btc_wallet_amount + $amount_sent;

                DB::beginTransaction();

                try {
                    $isUpdateProfit 		= $this->_walletBusiness->updateReferralProfit(Auth::user()->id, $referral_profit);
	                // $isUpdateWallet 		= $this->_walletBusiness->updateWalletAmount(Auth::user()->id, $wallet_amount);
                    $isInsertTransaction    = $this->_walletBusiness->handleTransactionUpdate($amount_sent, 'Direct Bonus Withdraw', 'Pending');

                    DB::commit();
                } catch (\Exception $e) {
                    DB::rollback();
                    return redirect()->route('dashboard.commission')
                        ->with('error', 'Something went wrong!!!!');
                }

                return redirect()->route('dashboard.commission')->with('message', 'Your profit was executed, please wait for 24h until it is sent to your wallet.');
	        }
        } catch (\Exception $e) {
            \Log::error('Dashboard\CommissionController::postCommission : ' . $e->getMessage());

            return redirect()->route('dashboard.commission')
                    ->with('error', 'Something went wrong!!!!');
        }
    }
 }
