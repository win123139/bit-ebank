<?php

namespace App\Http\Requests\DashBoard;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => 'required',
            'lastname'  => 'required',
            'birthday'  => 'required|date',
        ];
    }

    public function messages()
    {
        return [
            'firstname.required'                => 'First Name Required!!',
            'lastname.required'                 => 'Last Name Required!!',
            'birthday.required'                 => 'Birthday required!!',
            'birthday.date'                     => 'Birthday format wrong!!',
        ];
    }
}
