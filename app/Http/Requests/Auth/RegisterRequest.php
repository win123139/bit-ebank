<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'gender'                => 'required|integer|min:1|max:3',
            'firstname'             => 'required',
            'lastname'              => 'required',
            'birthday'              => 'required|date|date_format:"Y-m-d"',
            'ccno'                  => 'required',
            'email'                 => 'required|email|unique:user,email',
            'country'               => 'required',
            'password'              => 'required|confirmed|min:5',
            'password_confirmation' => 'required|min:5',
            'tnc'                   => 'required|accepted'
        ];
    }

    public function messages()
    {
        return [
            'gender.required'                => 'Choose your gender!!',
            'gender.integer'                 => 'Gender is invalid!!',
            'gender.min'                     => 'Gender is invalid!!',
            'gender.max'                     => 'Gender is invalid!!',
            'firstname.required'             => 'First name is required!!',
            'lastname.required'              => 'Last name is required!!',
            'birthday.required'              => 'Birthday is required!!',
            'birthday.date'                  => 'Birthday is invalid!!',
            'birthday.date_format'           => 'Format birthday is invalid!! (dd-mm-yyyy)!!',
            'ccno.required'                  => 'Ccno is required!!',
            'email.required'                 => 'Email is required!!',
            'email.email'                    => 'Email is invalid!!',
            'email.unique'                   => 'This email has been used, please try another one.',
            'country.required'               => 'Country is required!!',
            'password.required'              => 'Password is required!!',
            'password.min'                   => 'Password must be of minimum 5 character lenght!!',
            'password.confirmed'             => 'Password confirm is invalid!!',
            'password_confirmation.required' => 'Password confirm is required',
            'password_confirmation.min'      => 'Password confirm must be of minimum 5 character lenght!!',
            'tnc.required'                   => 'Please accept our temp!!',
            'tnc.accepted'                   => 'Please accept our temp!!',
        ];
    }
}
