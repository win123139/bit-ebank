<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Deposit extends Model
{
    protected $table = "deposit";

    protected $casts = [
        'id'      => 'string',
        'amount'  => 'double',
        'rate'    => 'integer',
        'period'  => 'integer',
        'user_id' => 'string',
        'status'  => 'string'
    ];

    protected $fillable = [
        'id', 'start_date', 'end_date', 'last_date_withdraw', 'amount', 'rate', 'period', 'user_id', 'status'
    ];


    /**
     * Relation belong to User
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * Format Start Date
     *
     * @param string $format
     *
     * @return string
     */
    public function getStartDate($format = 'd/m/Y')
    {
        $startDate = new Carbon($this->start_date);

        return $startDate->format($format);
    }

    /**
     * Format End Date
     *
     * @param string $format
     *
     * @return string
     */
    public function getEndDate($format = 'd/m/Y')
    {
        $endDate = new Carbon($this->end_date);

        return $endDate->format($format);
    }

    /**
     * Format Last Date Withdraw
     *
     * @param string $format
     *
     * @return string
     */
    public function getLastPayDate($format = 'd/m/Y')
    {
        $endDate = new Carbon($this->last_date_withdraw);

        return $endDate->format($format);
    }

    public function getDepositDate($format = 'd/m/Y', $addDate = 30)
    {
        $depositDate = new Carbon($this->start_date);

        return $depositDate->addDays($addDate)->format($format);
    }

    public function getPayDate($format = 'd/m/Y', $addDate = 30)
    {
        if(is_null($this->last_date_withdraw)){
            $depositDate = new Carbon($this->start_date);
        } else {
            $depositDate = new Carbon($this->last_date_withdraw);
        }

        return $depositDate->addMonth()->addDay()->format($format);
    }

    public function getProfit()
    {
        $rate = (int)$this->rate;
        $amount = (double)$this->amount;

        return ($amount * $rate) / 100;
    }

    public function getInterest()
    {
        if ($this->type == 0) {
            $amount = 0.2;
        } else {
            $amount = (double)$this->type;
        }        

        return ($amount * 20) / 100;
    }
}
