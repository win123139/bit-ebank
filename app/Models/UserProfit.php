<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserProfit extends Model
{
    protected $table = "user_profit";

    protected $primaryKey = 'user_id';

    protected $casts = [
        'user_id'           => 'string',
        'referral_profit'   => 'double',
        'binary_profit'     => 'double',
        'deposit_profit'    => 'double',
        'btc_wallet_amount' => 'double',
    ];

    protected $fillable = [
        'user_id', 'referral_profit', 'binary_profit', 'deposit_profit', 'btc_wallet_amount', 'last_update'
    ];

    /**
     * User Belong to one User
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'user_id');
    }
}
