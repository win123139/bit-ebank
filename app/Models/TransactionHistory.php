<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class TransactionHistory extends Model
{
    protected $table = "transaction_history";
    public $timestamps = true;
    public $incrementing = false;

    public function user()
    {
        return $this->belongsTo(User::class, 'usid');
    }

    public function getDate()
    {
        $date = new Carbon($this->date);

        return $date->format('d-m-Y');
    }

    public function getStatusView()
    {
        if ($this->status == 'Pending') {
            return 'warning';
        } elseif ($this->status == 'Completed') {
            return 'info';
        } elseif ($this->status == 'Pending') {
            return 'danger';
        }

        return '';
    }
}
