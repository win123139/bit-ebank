<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BinaryDownline extends Model
{

    protected $table = "binary_downline";

    protected $primaryKey = 'accountno';

    protected $casts = [
        'accountno'      => 'string',
        'name'           => 'string',
        'left_acc'       => 'string',
        'right_acc'      => 'string',
        'binarylevel'    => 'integer',
        'deposit_amount' => 'double'
    ];

    protected $fillable = [
        'accountno', 'name', 'left_acc', 'right_acc', 'binarylevel', 'deposit_amount'
    ];

    protected static $depositValue = array();

    /**
     * Get Children Left Tree
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function leftAccount()
    {
        return $this->belongsTo(self::class, 'left_acc', 'accountno');
    }

    /**
     * Get Children Right Tree
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function rightAccount()
    {
        return $this->belongsTo(self::class, 'right_acc', 'accountno');
    }


    /**
     * Get Parent Tree
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function parent()
    {
        return $this->hasMany(self::class, 'left_acc', 'accountno');
    }

    /**
     * Get Binary Tree
     *
     * @return \Illuminate\Support\Collection
     */
    public function getBinaryTree()
    {
        $rightAcc = null;
        if (!empty($this->right_acc)) {
            $rightAcc = $this->rightAccount()->first();
        }

        $leftAcc = null;
        if (!empty($this->left_acc)) {
            $leftAcc = $this->leftAccount()->first();
        }

        return collect([
            'left'  => $leftAcc,
            'right' => $rightAcc
        ]);
    }


    /**
     * Get Deposit
     *
     * @param $binaryDownline BinaryDownline
     *
     * @return int
     */
    public function getDeposit($binaryDownline)
    {
        if (!array_key_exists($binaryDownline->accountno, self::$depositValue)) {

            $sum = 0;
            $users = $binaryDownline->getBinaryTree();

            foreach ($users as $key => $user) {
                if (!is_null($users->get($key))) {
                    $sum += $user->deposit_amount;
                    $sum += $this->getDeposit($user);
                }
            }
            self::$depositValue[$binaryDownline->accountno] = $sum;
        }

        return self::$depositValue[$binaryDownline->accountno];
    }

    /**
     * Get Left Deposit By Binary User
     *
     * @return mixed
     */
    public function getLeftDeposit()
    {
        /** @var $leftUser BinaryDownline */
        $leftUser = null;
        if (!empty($this->left_acc)) {
            $leftUser = $this->leftAccount()->first();
        }

        if (is_null($leftUser)) {
            return 0;
        }

        return $this->deposit_amount + $this->getDeposit($leftUser);
    }

    /**
     * Get Right Deposit By Binary User
     *
     * @return mixed
     */
    public function getRightDeposit()
    {
        /** @var $rightUser BinaryDownline */
        $rightUser = null;
        if (!empty($this->right_acc)) {
            $rightUser = $this->rightAccount()->first();
        }
        if (is_null($rightUser)) {
            return 0;
        }

        return $this->deposit_amount + $this->getDeposit($rightUser);
    }

}
