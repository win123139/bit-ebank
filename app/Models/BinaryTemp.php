<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class BinaryTemp extends Model
{
    protected $table = "binary_temp";

    protected $primaryKey = 'accountno';

    protected $casts = [
        'accountno'         => 'string',
        'last_left_venue'   => 'double',
        'last_right_venue'  => 'double',
        'last_date_payment' => 'integer'
    ];

    protected $fillable = [
        'accountno', 'last_left_venue', 'last_right_venue', 'last_date_payment'
    ];


    /**
     * Relationship belong to user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'accountno', 'accountno');
    }

    public function getLastDatePayMent($format = 'd/m/Y')
    {
        return Carbon::createFromTimestamp($this->last_date_payment)->format($format);
    }
}
