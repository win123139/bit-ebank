<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;

class ContactEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $contactEmail;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Collection $contactEmail)
    {
        $this->contactEmail = $contactEmail;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $contactEmail = $this->contactEmail;

        return $this->subject('Contact Email')->view('dashboard.contact.email', compact('contactEmail'));
    }
}
